#pragma once

#include "locks_impl.h"
#include <bits/stdc++.h>
using namespace std;

//const int MAX_LEVEL = 44;
//const int MAX_THREADS = 1000;
//const int PADDING_SIZE = 128;

namespace {
    const int INITIAL_MAX_QUERIES = 400;
}


template<typename K, typename V>
struct NodeData
{
    K key;
    V value;
};

template<typename K, typename V>
struct Node {
    Node() = delete;
    Node(K _minKey, K _maxKey, int _initSize, int _levelSize, int64_t _accesses)
    : initSize(_initSize)
    , maxUpdates(std::max((int64_t)100, _accesses + 1))
    , levelSize(_levelSize) {
        children = new Node<K, V>* [levelSize + 1];
        marks = new bool[levelSize + 2];
        accesses = new int64_t[levelSize + 2];
        values = new NodeData<K, V> [levelSize + 2];
        id = new int[levelSize];
        for (int i = 0; i < levelSize + 1; i++) {
            children[i] = NULL;
        }
        for (int i = 0; i < levelSize + 2; i++) {
            marks[i] = false;
            accesses[i] = 0;
            // TODO: use the same value for accesses and mark (negative when deleted)
        }
        for (int i = 0; i < levelSize; i++) {
            id[i] = -1;
        }
        values[0].key = _minKey;
        values[levelSize + 1].key = _maxKey;
    }
    const int initSize = 1, levelSize = 1;
    int64_t maxUpdates = 1;
    int64_t numUpdates = 0;
    NodeData<K, V>* values = NULL;
    bool* marks = NULL;
    Node<K, V>** children = NULL;
    int* id = NULL;
    int64_t* accesses = NULL;
    int findOnLevel(const K & key);
    void getTreeData(std::vector<NodeData<K, V> > & v, std::vector<int64_t> & a,
                     std::vector<int64_t> & pref);
    void countIds();

    ~Node() {
        if (children) delete[] children;
        if (marks) delete[] marks;
        if (values) delete[] values;
        if (id) delete[] id;
        if (accesses) delete[] accesses;
    }
};

template<typename K, typename V, class RecordManager>
class InterpolationSearchTree {
private:
    const int LEAF_SIZE = 12;
    int numValues = 0;
    V noValue;
    Node<K, V>* root;
public:
    InterpolationSearchTree(const V noValue, const K minKey, const K maxKey);

    ~InterpolationSearchTree();

    V find(const int tid, const K &key);

    bool contains(const int tid, const K &key);

    V insertIfAbsent(const int tid, const K &key, const V &value);

    V erase(const int tid, const K &key);

    bool validate();

    void printDebuggingDetails();

    void initThread(const int tid);

    void deinitThread(const int tid);
    
    Node<K, V>* getRoot();

private:
    void updateNode(const int tid, Node<K, V> * nodeToUpdate, const int childNumToUpdate);
    Node<K, V>* buildIdealTree(const int tid, int left, int right, K minKey, K maxKey,
    vector<NodeData<K, V>> const &data, 
    vector<int64_t> const &a,
    vector<int64_t> const &pref);
};

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::buildIdealTree(const int tid, 
    int l, int r, K minKey, K maxKey,
    vector<NodeData<K, V>> const &data,
    vector<int64_t> const &accesses,
    vector<int64_t> const &pref) {
    int len = r - l;
    if (len <= LEAF_SIZE) {
        Node<K, V>* root = new Node<K, V>(minKey, maxKey, len, len, pref[r] - pref[l]);
        for (int i = 0; i < len; i++) {
            root->values[i + 1] = data[l + i];
            root->accesses[i + 1] = accesses[l + i];
        }
        
        root->countIds();
        return root;
    }

    int curSize = 0;
    int blockSize = int(sqrt(double(len))) / 6 + 1;
    int levelSize = len / blockSize;
    int64_t numAcc = (pref[r] - pref[l]) / (levelSize + 1);
    int cnt = 0;
    int cur_pos = l;
    while (cur_pos < r && pref[r] - pref[cur_pos] >= numAcc) {
        int ql = cur_pos, qr = r;
        while (qr - ql > 1) {
            int mid = (ql + qr) / 2;
            if (pref[mid] - pref[cur_pos] < numAcc) {
                ql = mid;
            } else {
                qr = mid;
            }
        }
        cnt++;
        cur_pos = qr;
    }
    Node<K, V>* root = new Node<K, V>(minKey, maxKey, len, cnt, pref[r] - pref[l]);
    cnt = 0;
    cur_pos = l;
    while (cur_pos < r && pref[r] - pref[cur_pos] >= numAcc) {
        int ql = cur_pos, qr = r;
        while (qr - ql > 1) {
            int mid = (ql + qr) / 2;
            if (pref[mid] - pref[cur_pos] < numAcc) {
                ql = mid;
            } else {
                qr = mid;
            }
        }
        cnt++;
        root->accesses[cnt] = ql;
        cur_pos = qr;
    }
    int lst_pos = l;
    for (int i = 1; i <= cnt; i++) {
        int cposs = root->accesses[i];
        root->accesses[i] = accesses[cposs];
        root->values[i] = data[cposs];
        if (lst_pos != cposs)
            root->children[i - 1] = buildIdealTree(tid, lst_pos, cposs, root->values[i - 1].key, data[cposs].key, data, accesses, pref);
        lst_pos = cposs + 1;
    }
    if (lst_pos != r) {
        root->children[cnt] = buildIdealTree(tid, lst_pos, r, root->values[cnt].key, maxKey, data, accesses, pref);
    }
    root->countIds();
    return root;
}

template <typename K, typename V, class RecordManager>
InterpolationSearchTree<K, V, RecordManager>::InterpolationSearchTree(const V noValue, const K minKey, const K maxKey)
    : noValue(noValue)
    , root(new Node<K, V>(minKey, maxKey, 0, 0, 1000)) {
}

template <typename K, typename V, class RecordManager>
bool InterpolationSearchTree<K, V, RecordManager>::contains(const int tid, const K &key) {
    return find(tid, key) == noValue;
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::find(const int tid, const K &key) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    Node<K, V> *nodeToUpdate = NULL;
    int pos = 0;
    V result = noValue;
    int childNumToUpdate = 0;

    while (true) {
        cur->numUpdates++;
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }
        pos = cur->findOnLevel(key);
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                cur->accesses[pos]++;
                result = (cur->values[pos]).value;
            }
            break;
        }
        if ((cur->children[pos]) == NULL) {
            break;
        }
        prev = cur;
        cur = (cur->children[pos]);
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);    
    }
    return result;
}

template <typename K, typename V>
void Node<K, V>::countIds() {
    int cl = 0;
    for (int i = 0; i < (levelSize); i++) {
        K key = values[0].key + (long long)(values[levelSize + 1].key - values[0].key) * i / (levelSize);
        while ((values[cl]).key <= key) {
            cl++;
        }
        id[i] = cl - 1;
    }    
}

template <typename K, typename V>
int Node<K, V>::findOnLevel(const K & key) {
    if (levelSize == 0) return 0;
    int posApprox = id[(long long)levelSize * (key - values[0].key) / (values[levelSize + 1].key - values[0].key)];
    int kp = 1;
    while (values[posApprox + 1].key <= key && kp <= levelSize + 2) {
        kp *= 2;
        posApprox++;
    }
    if (kp > levelSize + 2) {
        int l = 0, r = levelSize + 2;
        while (r - l > 1) {
            int mid = (l + r) / 2;
            if (values[mid].key == key)
                return mid;
            if (values[mid].key < key) {
                l = mid;
            } else
                r = mid;
        }
        return l;
    } else
        return posApprox;
}

template <typename K, typename V>
void Node<K, V>::getTreeData(std::vector<NodeData<K, V>> & data, 
                             std::vector<int64_t> & a,
                             std::vector<int64_t> & pref) {
    for (int i = 0; i <= levelSize; i++) {
        if (i > 0 && !marks[i]) {
            int k = pref.back() + accesses[i];
            pref.push_back(k);
            data.push_back(values[i]); 
            a.push_back(accesses[i]);
        }
        if (children[i] != NULL) children[i]->getTreeData(data, a, pref);
    }
}

namespace {
    template <typename K, typename V>
    void delete_tree(Node<K, V>* node) {
        if (node->children) {
            for (int i = 0; i <= node->levelSize; i++)
                if (node->children[i]) delete_tree(node->children[i]);
        }
        delete node;
    }
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::updateNode(const int tid, Node<K, V> *nodeToUpdate, const int childNumToUpdate) {
    auto nd = nodeToUpdate->children[childNumToUpdate];
    vector<NodeData<K, V>> vec(0);
    vector<int64_t> pref, a;
    pref.push_back(0);
    nd->getTreeData(vec, a, pref);
    nodeToUpdate->children[childNumToUpdate] = buildIdealTree(tid, 0, 
    int(vec.size()), nd->values[0].key, nd->values[(nd->levelSize) + 1].key, vec, a, pref);
    delete_tree(nd);
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::insertIfAbsent(const int tid, const K &key, const V &value) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    int pos = 0;
    Node<K, V> *nodeToUpdate = NULL;
    int childNumToUpdate = 0;
    V result = noValue;

    while (true) {
        cur->numUpdates++;
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }       
        pos = cur->findOnLevel(key);
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                result = (cur->values[pos]).value;
            } else {
                cur->marks[pos] = false;
                (cur->values[pos]).value = value;
                cur->accesses[pos] = 1;
                numValues++;
            }
            break;
        }
        if ((cur->children[pos]) == NULL) {
            auto nd = new Node<K, V>((cur->values[pos]).key, (cur->values[pos + 1]).key, 1, 1, INITIAL_MAX_QUERIES);
            nd->values[1].value = value;
            nd->values[1].key = key;
            nd->accesses[1] = 1;
            numValues++;
            nd->countIds();
            cur->children[pos] = nd;
            break;
        }
        prev = cur;
        cur = (cur->children[pos]);
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);    
    }
    return result;
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::erase(const int tid, const K &key) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    int pos = 0;
    Node<K, V> *nodeToUpdate = NULL;
    int childNumToUpdate = 0;
    V result = noValue;

    while (true) {
        cur->numUpdates++;
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }       
        pos = cur->findOnLevel(key);
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                result = (cur->values[pos]).value;
                cur->marks[pos] = true;
                cur->accesses[pos] = 1;
                numValues--;
            }
            break;
        }
        if ((cur->children[pos]) == NULL) {
            break;
        }
        prev = cur;
        cur = (cur->children[pos]);
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);    
    }
    return result;
}

template <typename K, typename V, class RecordManager>
bool InterpolationSearchTree<K, V, RecordManager>::validate() {
    //TODO
    return true;
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::initThread(const int tid) {
//    if (init[tid]) return;
//    else init[tid] = !init[tid];
//    recordManager->initThread(tid);
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::printDebuggingDetails() {
    //TODO
    return;
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::deinitThread(const int tid) {
//    if (!init[tid]) return;
//    else init[tid] = !init[tid];
//    recordManager->deinitThread(tid);
}

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::getRoot() {
    return root;
}

template <typename K, typename V, class RecordManager>
InterpolationSearchTree<K, V, RecordManager>::~InterpolationSearchTree() {
//    recordManager->printStatus();
//    delete recordManager;
}
