//
// Created by demon1999 on 22.11.19.
//
#include <bits/stdc++.h>

using namespace std;

const int NO_VALUE = -1;
const int MAX_KEY = 1000;
const int OPERATIONS = 10000;
const int THREAD_KEYS = 10;

int main() {
    srand(time(0));
    long long min_key = 0;
    long long max_key = MAX_KEY;
    std::cout << OPERATIONS << " " << MAX_KEY << "\n";
    for (int i = 0; i < OPERATIONS; i++) {
        int k = rand() % (MAX_KEY - 2) + 1;
        int op = rand() % 3;
        std::cout << op << " " << k << " " << rand() % (MAX_KEY - 2) + 1 << "\n";
    }
}

