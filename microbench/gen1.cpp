//
// Created by demon1999 on 22.11.19.
//
#include <bits/stdc++.h>
const int MAX_KEY = 10000;
const int STRESS_MAX_THREADS = 4;


using namespace std;

const int OPERATIONS = 10000;
const int THREAD_KEYS = 500;

vector<vector<pair<pair<int, int>, int> > > operations;

int main(int argc, char **argv) {
    // srand(2399);
    srand(atoi(argv[1]));
    //  std::cout << atoi(argv[1]) << " kek\n";
    int numThreads = rand() % STRESS_MAX_THREADS + 1;
    // cout << "Number of threads is " << numThreads << "\n";
    //exit(0);
    set<long long> ks;
    ks.insert(0);
    vector<long long> keys;
    long long min_key = 0;
    long long max_key = MAX_KEY;
    for (int i = 0; i < numThreads * THREAD_KEYS; i++) {
        long long k = rand() % MAX_KEY;
        while (ks.find(k) != ks.end()) {
            k = rand() % MAX_KEY;
        }
        keys.push_back(k);
        ks.insert(k);
    }
    operations.resize(numThreads, vector<pair<pair<int, int>, int> >(0));
    for (int i = 0; i < OPERATIONS; i++) {
        int op_type = rand() % 3;
        int th = rand() % numThreads;
        int key = keys[(rand() % THREAD_KEYS) + THREAD_KEYS * th];
        operations[th].push_back(make_pair(make_pair(op_type, key), rand() % MAX_KEY + 1));
    }
    cout << numThreads << "\n";
    for (size_t i = 0; i < operations.size(); i++) {
        cout << int(operations[i].size()) << "\n";
        int k = 0;
        for (auto op : operations[i]) {
            // k++;
            // if (k > 10) break;
            if (op.first.first == 0)
                cout << "0 ";
            if (op.first.first == 1)
                cout << "1 ";
            if (op.first.first == 2)
                cout << "2 ";
            cout << op.first.second << " " << op.second << "\n";
        }
    }
}

