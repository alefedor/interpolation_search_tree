//
// Created by demon1999 on 22.11.19.
//
#include <bits/stdc++.h>
#include "globals_extern.h"
#include "random_fnv1a.h"
#include "plaf.h"
#include "binding.h"
#include "papi_util_impl.h"
#include "rq_provider.h"
#include "keygen.h"
#include "adapter.h"
#define VALUE_TYPE int
#define KEY_TYPE int
#define DS_ADAPTER_T ds_adapter<KEY_TYPE, VALUE_TYPE, RECLAIM<>, ALLOC<>, POOL<> >

const int MAX_KEY = 10000;
const int STRESS_MAX_THREADS = 4;


Random64 rngs[MAX_THREADS_POW2];
using namespace std;

const int NO_VALUE = 0;

DS_ADAPTER_T* orderedSet = new DS_ADAPTER_T(STRESS_MAX_THREADS, 0, MAX_KEY, NO_VALUE, rngs);

vector<vector<pair<pair<int, int>, int> > > operations;

mutex my_lock;
bool is_ok = true;

bool disj_keys = false;

void task(int j) {
    map<int, int> s;
    size_t pos = 0;
    orderedSet->initThread(j);
    for (pos = 0; pos < operations[j].size(); pos++) {
        int key = operations[j][pos].first.second;
        int op_type = operations[j][pos].first.first;
        int val =  operations[j][pos].second;
        if (op_type == 0) {
            auto value = orderedSet->find(j, key);
            if (disj_keys) {
                if (value != NO_VALUE && (s.find(key) == s.end())) {
                    my_lock.lock();
                    std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << value << "\n";
                    is_ok = false;
                    my_lock.unlock();
                }
                if ((s.find(key) != s.end()) && value != s[key]) {
                    my_lock.lock();
                    std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " <<  s[key] << value << "\n";
                    is_ok = false;
                    my_lock.unlock();
                }
            }
        }
        if (op_type == 1) {
            int value = orderedSet->erase(j, key);
            if (disj_keys) {
                if ((s.find(key) != s.end()) && value != s[key]) {
                    my_lock.lock();
                    std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << s[key] << value << "\n";
                    is_ok = false;
                    my_lock.unlock();
                }
                if (value != NO_VALUE && (s.find(key) == s.end())) {
                    my_lock.lock();
                    std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << value << "\n";
                    is_ok = false;
                    my_lock.unlock();
                }
                if (s.find(key) != s.end()) {
                    s.erase(key);
                }
            }
        }
        if (op_type == 2) {
            int value = orderedSet->insertIfAbsent(j, key, val);
            if (disj_keys) {
                if ((s.find(key) == s.end()) && value != NO_VALUE) {
                    my_lock.lock();
                    std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << value << "\n";
                    is_ok = false;
                    my_lock.unlock();
                }
                if ((s.find(key) != s.end()) && value != s[key]) {
                    my_lock.lock();
                    std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << s[key] << value << "\n";
                    is_ok = false;
                    my_lock.unlock();
                }
                if (s.find(key) == s.end())
                    s[key] = val;
            }
        }
    }
}

int main(int argc, char **argv) {
    if (argc > 1 && argv[1] == "disj_keys") {
        disj_keys = true;
    }
    int numThreads;
    cin >> numThreads;
    long long min_key = 0;
    long long max_key = MAX_KEY;
    operations.resize(numThreads, vector<pair<pair<int, int>, int> >(0));
    for (int th = 0; th < numThreads; th++) {
        int cnt;
        cin >> cnt;
        for (int i = 0; i < cnt; i++) {
            int op_type, key, val;
            cin >> op_type >> key >> val;
            operations[th].push_back(make_pair(make_pair(op_type, key), val));
        }
    }
    for (int iko = 0; iko < 1; iko++) {
        std::cout << "\n\n\n\nITERATION " << iko << "\n";
        orderedSet = new DS_ADAPTER_T(STRESS_MAX_THREADS, 0, MAX_KEY, NO_VALUE, rngs);
        thread a[numThreads];
        for (int i = 0; i < numThreads; i++) {
            a[i] = thread(task, i);
        }
        for (int i = 0; i < numThreads; i++) {
            a[i].join();
        }
        
        if (is_ok) {
            cout << "You are great!!!\n";
        } else {
            // assert(0);
            cout << "Something went wrong!!!\n";
            exit(0);
        }
        orderedSet->validateStructure();
    }
}

