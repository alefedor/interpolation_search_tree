//
// Created by demon1999 on 07.02.20.
//

#include <bits/stdc++.h>
#include "globals_extern.h"
#include "random_fnv1a.h"
#include "plaf.h"
#include "binding.h"
#include "papi_util_impl.h"
#include "rq_provider.h"
#include "keygen.h"
#include "adapter.h"

#define DS_ADAPTER_T ds_adapter<KEY_TYPE, VALUE_TYPE, RECLAIM<>, ALLOC<>, POOL<> >

Random64 rngs[MAX_THREADS_POW2];

const int MAX_KEY = 10000000;
const int SCENARIO_SIZE = 80000000;
typedef long long ll;

#define VALUE_TYPE int
#define KEY_TYPE int
using namespace std;

namespace {
    int random_int(int min, int max) {
        static thread_local std::mt19937* gen = nullptr;
        if (!gen) gen = new std::mt19937(456457);
        uniform_int_distribution<int> distribution(min, max);
        return distribution(*gen);
    }

    std::mt19937 gen;

    double random_double(double max) {
        std::uniform_real_distribution<double> distribution(0.0, max);
        return distribution(gen);
    }
}

int NO_VALUE = -1;

DS_ADAPTER_T* database = new DS_ADAPTER_T(1, 0, MAX_KEY + 1, NO_VALUE, rngs);


int num_threads = 1, x = 100, y = 100, seconds = 1, presecs = 1, prefill = 10, cops = 1;

long long sum_lengths, sum_ops;
vector<int> important, rest;


bool zipf = false;

class ElapsedTimer {
private:
    bool calledStart = false;
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
public:
    void startTimer() {
        calledStart = true;
        start = std::chrono::high_resolution_clock::now();
    }
    int64_t getElapsedMillis() {
        if (!calledStart) {
            printf("ERROR: called getElapsedMillis without calling startTimer\n");
            exit(1);
        }
        auto now = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<std::chrono::milliseconds>(now - start).count();
    }
};


int64_t run_benchmark(std::vector<int> const& scenario) {
    int64_t garbage = random_int(0, 54363);

    ElapsedTimer timer;
    timer.startTimer();
    for (int i = 0; i < scenario.size(); i++)
        garbage += database->find(0, scenario[i]);
    if (garbage % 5436562 == 56) std::cout << "side effect";
    return timer.getElapsedMillis();
}

bool ideal = false;

struct ZipfGenerator {
    std::vector<int> *keys;
    std::vector<double> cumulative_weights;
    double total_weight;

    ZipfGenerator(std::vector<int>& keys) : keys(&keys) {
        double sum = 0.0;
        for (int i = 0; i < keys.size(); i++) {
            sum += 1.0 / (1 + i);
            cumulative_weights.push_back(sum);
        }
        total_weight = sum;
    }

    int generate() {
        double rnd = random_double(total_weight);
        int l = -1;
        int r = keys->size() - 1;
        while (l + 1 < r) {
            int m = (l + r) / 2;
            if (cumulative_weights[m] + 1e-9 > rnd)
                r = m;
            else
                l = m;
        }
        return (*keys)[r];
    }
};

int main(int argc, char **argv) {
    zipf = false;
    double alpha = 0;
    // for (int i = 1; i < argc; i++) {
    //     std::cout << argv[i] << '\n';
    // }
    // exit(0);
    for (int i = 1; i < argc; i += 2) {
        // std::cout << argv[i] << "\n";
        if (string(argv[i]) == "x") {
            if (argc == i + 1) {
                std::cout << argv[i] << "\n";
                std::cout << "Wrong arguments\n";
                exit(0);
            }
            x = atoi(argv[i + 1]);
        } else if (string(argv[i]) == "y") {
            y = atoi(argv[i + 1]);
        } else if (string(argv[i]) == "threads") {
            num_threads = atoi(argv[i + 1]);
        } else if (string(argv[i]) == "secs") {
            seconds = atoi(argv[i + 1]);
        } else if (string(argv[i]) == "presecs") {
            presecs = atoi(argv[i + 1]);
        } else if (string(argv[i]) == "prefill") {
            prefill = atoi(argv[i + 1]);
        } else if (string(argv[i]) == "ideal") {
            ideal = true;
        } else if (string(argv[i]) == "zipf") {
            zipf = true;
            alpha = atof(argv[i + 1]);
        } else {
            std::cout << argv[i] << "\n";
            std::cout << "Wrong arguments\n";
            exit(0);
        }
    }

//    database->setCops(0, cops);

    vector<int> all_keys;
    for (int i = 0; i < MAX_KEY; i++)
        all_keys.push_back(i);

    std::shuffle(all_keys.begin(), all_keys.end(), std::mt19937(248582));


    for (int i = 0; i < prefill; i++) {
        if (zipf || i * 100ll / (long long)prefill < y)
            important.push_back(all_keys[i]);
        else
            rest.push_back(all_keys[i]);
    }

    std::shuffle(all_keys.begin(), all_keys.begin() + prefill, std::mt19937(758453));
    for (int i = 0; i < prefill; i++) {
        database->insertIfAbsent(0, all_keys[i], random_int(0, 10));
    }

    ZipfGenerator zipfGen(important);

    std::vector<int> scenario;
    scenario.resize(SCENARIO_SIZE);

    for (int i = 0; i < SCENARIO_SIZE; i++) {
        if (zipf) {
            scenario[i] = zipfGen.generate();
        } else {
            if (random_int(0, 99) < x) {
                scenario[i] = important[random_int(0, important.size() - 1)];
            } else {
                scenario[i] = rest[random_int(0, rest.size() - 1)];
            }
        }
    }

    int64_t millis = run_benchmark(scenario);

//    vector<thread> a(num_threads);
//
//    for (int i = 0; i < num_threads; i++) {
//        a[i] = thread(task, i);
//
//        cpu_set_t cpuset;
//        CPU_ZERO(&cpuset);
//        CPU_SET(i, &cpuset);
//        int rc = pthread_setaffinity_np(a[i].native_handle(),
//                                        sizeof(cpu_set_t), &cpuset);
//    }

    cout << fixed << setprecision(6);

    std::cout << database->getName() << "," << x << "," << y << "," << (zipf ? "zipf" : "random") << "," << SCENARIO_SIZE / (double ) millis << std::endl;
//    std::cout << "parameters:\n";
//    std::cout << "threads: " << num_threads << "\n";
//    std::cout << "zipf: " << zipf << "\n";
//    std::cout << "alpha: " << alpha << "\n";
//    std::cout << "x: " << x << "\n";
//    std::cout << "y: " << y << "\n";
//    std::cout << "prefill: " << prefill << "\n";
//    std::cout << "secs: " << seconds << "\n";
//    std::cout << "upd ops: " << cops << "\n";
//    std::cout << "results:\n";
//    // std::cout << "h: " << 0 /*(database->getHeight())*/ << ", ops: " << sum_ops << ", sumLengths: " << double(sum_lengths) / double(sum_ops) << "\n";
//    std::cout << double(sum_ops) / double(seconds) << "\n";
}

