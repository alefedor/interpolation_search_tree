#pragma once

#include "locks_impl.h"
#include <bits/stdc++.h>
using namespace std;

//const int MAX_LEVEL = 44;
//const int MAX_THREADS = 1000;
//const int PADDING_SIZE = 128;

template<typename K, typename V>
struct NodeData
{
    K key;
    V value;
    NodeData() {}
    NodeData(K _key, V _value) 
    : key(_key)
    , value(_value) {}
};

template<typename K, typename V>
struct Node {
    Node() = delete;
    Node(K _minKey, K _maxKey, int _initSize, int _levelSize)
    : initSize(_initSize)
    , maxUpdates(_initSize == _levelSize ? _initSize : 3 * _initSize / 4 + 1)
    , blockSize(_levelSize == 0 ? 1 : _initSize / _levelSize + 1)
    , levelSize(_initSize == _levelSize ? 2 * _levelSize + 1 : _levelSize)
    , idSize(_levelSize + 1) {
        if (_initSize == _levelSize) {
            values = new V[levelSize];
            keys = new K[levelSize + 2];
            keys[levelSize] = _minKey;
            keys[levelSize + 1] = _maxKey;
        } else {
            children = new Node<K, V>*[levelSize + 1];
            keys = new K[levelSize + 2];
            id = new int[idSize];
            for (int i = 0; i < levelSize + 1; i++) {
                children[i] = NULL;
            }
            for (int i = 0; i < idSize; i++) {
                id[i] = -1;
            }
            keys[0] = _minKey;
            keys[levelSize + 1] = _maxKey;
        }
    }
    Node(K _minKey, K _maxKey, int _initSize)
    : initSize(_initSize)
    , maxUpdates(_initSize * 1 / 2 + 1)
    , blockSize(initSize == 1 ? 1 : int(sqrt(double(initSize))) * 9 / 8 + 1)
    , levelSize(initSize / blockSize)
    , idSize(blockSize + 1) {
        children = new Node<K, V>*[levelSize + 1];
        keys = new K [levelSize + 2];
        id = new int[idSize];
        for (int i = 0; i < levelSize + 1; i++) {
            children[i] = NULL;
        }
        for (int i = 0; i < idSize; i++) {
            id[i] = -1;
        }
        keys[0] = _minKey;
        keys[levelSize + 1] = _maxKey;
    }
    const int initSize = 1, maxUpdates = 1;
    const int blockSize = 1, levelSize = 1;
    const int idSize = blockSize + 1;
    int numUpdates = 0, leafSize = 0;
    K* keys = NULL;
    V* values = NULL;
    Node<K, V>** children = NULL;
    int* id = NULL;
    bool isLeaf() {
        return (values != NULL);
    }
    int findOnLevel(const K & key);
    void countIds();
    void getTreeData(std::vector<NodeData<K, V> > & v);
    void printInfo(int level);

    ~Node() {
        if (children) delete[] children;
        if (keys) delete[] keys;
        if (values) delete[] values;
        if (id) delete[] id;
    }
};

template<typename K, typename V, class RecordManager>
class InterpolationSearchTree {
private:
    const int LEAF_SIZE = 12;
    V noValue;
    Node<K, V>* root;
public:
    InterpolationSearchTree(const V noValue, const K minKey, const K maxKey);

    ~InterpolationSearchTree();

    V find(const int tid, const K &key);

    bool contains(const int tid, const K &key);

    V insertIfAbsent(const int tid, const K &key, const V &value);

    V erase(const int tid, const K &key);

    bool validate();

    void printDebuggingDetails();

    void initThread(const int tid);

    void deinitThread(const int tid);
    
    Node<K, V>* getRoot();

private:
    void updateNode(const int tid, Node<K, V> * nodeToUpdate, const int childNumToUpdate);
    Node<K, V>* buildIdealTree(const int tid, int left, int right, K minKey, K maxKey, vector<NodeData<K, V>> const &data);
};

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::buildIdealTree(const int tid, 
    int l, int r, K minKey, K maxKey,
    vector<NodeData<K, V>> const &data) {
    Node<K, V>* root = new Node<K, V>(minKey, maxKey, int(r - l));
    int len = r - l;
    if (len <= LEAF_SIZE) {
        Node<K, V>* root = new Node<K, V>(minKey, maxKey, len, len);
        for (int i = 0; i < len; i++) {
            root->values[i] = data[l + i].value;
            root->keys[i] = data[l + i].key;
        }
        root->leafSize = len;
        return root;
    }
    int cl = l;
    int cnt, pos;
    auto blockSize = (root->blockSize);
    for (pos = l + blockSize - 1, cnt = 0; pos < r; cnt++, pos += blockSize) {
        if (cl <= pos) {
           (root->children[cnt]) = buildIdealTree(tid, cl, pos + 1, root->keys[cnt], pos + 1 < r ? data[pos + 1].key : maxKey, data);
        }
        cl = pos + 1;
        (root->keys[cnt + 1]) = data[pos].key;
    }
    if (cl < r) {
        (root->children[cnt]) = buildIdealTree(tid, cl, r, root->keys[cnt], maxKey, data);
    }
    root->countIds();
    return root;
}

template <typename K, typename V>
void Node<K, V>::countIds() {
    int cl = 1;
    for (int i = 0; i < (idSize); i++) {
        K key = keys[0] + (long long)(keys[levelSize + 1] - keys[0]) * i / idSize;
        while (cl < levelSize + 1 && (keys[cl]) < key) {
            cl++;
        }
        id[i] = cl - 1;
    }
}

template <typename K, typename V>
void Node<K, V>::printInfo(int level) {
    if (isLeaf()) {
        std::cout << "<begin_leaf>\n    ";
        for (int i = 0; i < leafSize; i++) {
            std::cout << "(key=" << keys[i] << ",value=" << values[i] << "), ";
        }
        std::cout << "\n<end_leaf>\n";
        return;
    } 
    std::cout << "<begin_inner>\n    ";
    for (int i = 0; i <= levelSize + 1; i++) {
        std::cout << "(key=" << keys[i] << "), ";
    }
    std:cout << "\n";
    for (int i = 0; i < idSize; i++) {
        std::cout << id[i] << " ";
    }
    std::cout << "\n";
    for (int i = 0; i <= levelSize; i++) {
        if (children[i] != NULL) {
            children[i]->printInfo(level + 1);
        }
    }
    std::cout << "<end_inner>\n";
}


template <typename K, typename V, class RecordManager>
InterpolationSearchTree<K, V, RecordManager>::InterpolationSearchTree(const V noValue, const K minKey, const K maxKey)
    : noValue(noValue)
    , root(new Node<K, V>(minKey, maxKey, 0)) {
    root->countIds();
}

template <typename K, typename V, class RecordManager>
bool InterpolationSearchTree<K, V, RecordManager>::contains(const int tid, const K &key) {
    return find(tid, key) == noValue;
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::find(const int tid, const K &key) {
    Node<K, V> *cur = root;
    int pos = 0;
    while (cur != NULL) {
        if (cur->isLeaf()) {
            for (int i = 0; i < cur->leafSize; i++) {
                if (cur->keys[i] == key)
                    return cur->values[i];
            }
            break;
        }
        pos = cur->findOnLevel(key);
        cur = (cur->children[pos]);
    }
    return noValue;
}


template <typename K, typename V>
int Node<K, V>::findOnLevel(const K & key) {
    assert(!isLeaf());
    int minKey = keys[0];
    int maxKey = keys[levelSize + 1];
    int posApprox = id[(long long)idSize * (key - minKey) / (maxKey - minKey)];
    int kp = 1;
    while (keys[posApprox + 1] < key && kp <= levelSize + 2) {
        kp *= 2;
        posApprox++;
    }
    if (kp > levelSize + 2) {
        int l = 0, r = levelSize + 2;
        while (r - l > 1) {
            int mid = (l + r) / 2;
            if (keys[mid] == key)
                return mid;
            if (keys[mid] < key) {
                l = mid;
            } else
                r = mid;
        }
        return l;
    } else
        return posApprox;
}

template <typename K, typename V>
void Node<K, V>::getTreeData(std::vector<NodeData<K, V>> & v) {
    if (isLeaf()) {
        for (int i = 0; i < leafSize; i++)
            v.push_back(NodeData<K, V>(keys[i], values[i]));
    } else
    for (int i = 0; i <= levelSize; i++) {
        if (children[i] != NULL) children[i]->getTreeData(v);
    }
}

namespace {
    template <typename K, typename V>
    void delete_tree(Node<K, V>* node) {
        if (node->children) {
            for (int i = 0; i <= node->levelSize; i++)
                if (node->children[i]) delete_tree(node->children[i]);
        }
        delete node;
    }
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::updateNode(const int tid, Node<K, V> *nodeToUpdate, const int childNumToUpdate) {
    auto nd = nodeToUpdate->children[childNumToUpdate];
    vector<NodeData<K, V>> vec(0);
    nd->getTreeData(vec);
    if (nd->isLeaf()) {
        nodeToUpdate->children[childNumToUpdate] = buildIdealTree(tid, 0, 
          int(vec.size()), nd->keys[nd->levelSize], nd->keys[(nd->levelSize) + 1], vec);
    } else
      nodeToUpdate->children[childNumToUpdate] = buildIdealTree(tid, 0, 
        int(vec.size()), nd->keys[0], nd->keys[(nd->levelSize) + 1], vec);
    delete_tree(nd);
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::insertIfAbsent(const int tid, const K &key, const V &value) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    int pos = 0;
    Node<K, V> *nodeToUpdate = NULL;
    int childNumToUpdate = 0;
    V result = noValue;

    while (true) {
        cur->numUpdates++;
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }       
        if (cur->isLeaf()) {
            bool ok = false;
            for (int i = 0; i < cur->leafSize; i++) {
                if ((cur->keys[i]) == key) {
                    result = cur->values[i];
                    ok = true;
                    break;
                }
                if ((cur->keys[i]) > key) {
                    for (int j = (cur->leafSize); j > i; j--) {
                        cur->keys[j] = (cur->keys[j - 1]);
                        cur->values[j] = (cur->values[j - 1]);
                    }
                    (cur->leafSize)++;
                    cur->keys[i] = key;
                    cur->values[i] = value;
                    ok = true;
                    break;
                }
            }
            if (!ok) {
                cur->keys[cur->leafSize] = key;
                cur->values[cur->leafSize] = value;
                (cur->leafSize)++;
            }
            break;
        }
        pos = cur->findOnLevel(key);
        prev = cur;
        cur = (cur->children[pos]);
        if (cur == NULL) {
            Node<K, V> *nd = new Node<K, V>((prev->keys[pos]), 
                (prev->keys[pos + 1]), LEAF_SIZE, LEAF_SIZE);
            nd->leafSize = 1;
            nd->keys[0] = key;
            nd->values[0] = value;
            prev->children[pos] = nd;
            break;
        }
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);
    }
    return result;
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::erase(const int tid, const K &key) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    int pos = 0;
    Node<K, V> *nodeToUpdate = NULL;
    int childNumToUpdate = 0;
    V result = noValue;

    while (true) {
        cur->numUpdates++;
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }
        if (cur->isLeaf()) {
            for (int i = 0; i < cur->leafSize; i++) {
                if ((cur->keys[i]) == key) {
                    result = (cur->values[i]);
                    for (int j = i; j + 1 < cur->leafSize; j++) {
                        cur->keys[j] = cur->keys[j + 1];
                        cur->values[j] = cur->values[j + 1]; 
                    }
                    (cur->leafSize)--;
                    break;
                }
            }
            break;
        }
        pos = cur->findOnLevel(key);
        prev = cur;
        cur = (cur->children[pos]);
        if (cur == NULL)
            break;
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);
    }

    return result;
}

template <typename K, typename V, class RecordManager>
bool InterpolationSearchTree<K, V, RecordManager>::validate() {
    //TODO
    return true;
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::initThread(const int tid) {
//    if (init[tid]) return;
//    else init[tid] = !init[tid];
//    recordManager->initThread(tid);
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::printDebuggingDetails() {
    std::cout << "<begin_debug>\n";
    root->printInfo(0);
    std::cout << "<end_debug>\n";
    return;
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::deinitThread(const int tid) {
//    if (!init[tid]) return;
//    else init[tid] = !init[tid];
//    recordManager->deinitThread(tid);
}

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::getRoot() {
    return root;
}

template <typename K, typename V, class RecordManager>
InterpolationSearchTree<K, V, RecordManager>::~InterpolationSearchTree() {
//    recordManager->printStatus();
//    delete recordManager;
}
