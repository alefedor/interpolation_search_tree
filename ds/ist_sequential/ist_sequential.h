#pragma once

#include "locks_impl.h"
#include <bits/stdc++.h>
using namespace std;

//const int MAX_LEVEL = 44;
//const int MAX_THREADS = 1000;
//const int PADDING_SIZE = 128;

template<typename K, typename V>
struct NodeData
{
    K key;
    V value;
};

template<typename K, typename V>
struct Node {
    Node() = delete;
    Node(K _minKey, K _maxKey, int _initSize, int _levelSize)
    : initSize(_initSize)
    , maxUpdates(3 * _initSize / 4 + 1)
    , blockSize(_levelSize == 0 ? 1 : _initSize / _levelSize + 1)
    , levelSize(_levelSize)
    , idSize(_levelSize + 1) {
//        std::cout << _minKey << " " << _maxKey << std::endl;
        children = new Node<K, V>*[levelSize + 1];
        marks = new bool[levelSize + 2];
        values = new NodeData<K, V> [levelSize + 2];
        id = new int[idSize];
        for (int i = 0; i < levelSize + 1; i++) {
            children[i] = NULL;
        }
        for (int i = 0; i < levelSize + 2; i++) {
            marks[i] = false;
        }
        for (int i = 0; i < idSize; i++) {
            id[i] = -1;
        }
        values[0].key = _minKey;
        values[levelSize + 1].key = _maxKey;
    }
    Node(K _minKey, K _maxKey, int _initSize)
    : initSize(_initSize)
    , maxUpdates(3 * _initSize / 4 + 1)
    , blockSize(initSize == 1 ? 1 : int(sqrt(double(initSize))) + 1) // * 0.85
    , levelSize(initSize / blockSize)
    , idSize(blockSize + 1) {
//        std::cout << _minKey << " " << _maxKey << std::endl;
        children = new Node<K, V>*[levelSize + 1];
        marks = new bool[levelSize + 2];
        values = new NodeData<K, V> [levelSize + 2];
        id = new int[idSize];
        for (int i = 0; i < levelSize + 1; i++) {
            children[i] = NULL;
        }
        for (int i = 0; i < levelSize + 2; i++) {
            marks[i] = false;
        }
        for (int i = 0; i < idSize; i++) {
            id[i] = -1;
        }
        values[0].key = _minKey;
        values[levelSize + 1].key = _maxKey;
    }
    const int initSize = 1, maxUpdates = 1;
    const int blockSize = 1, levelSize = 1;
    const int idSize = blockSize + 1;
    int numUpdates = 0;
    NodeData<K, V>* values;
    bool* marks;
    Node<K, V>** children;
    int* id;
    int findOnLevel(const K & key);
    void countIds();
    void getTreeData(std::vector<NodeData<K, V> > & v);
    void printInfo(int level);

    ~Node() {
        if (children) delete[] children;
        if (marks) delete[] marks;
        if (values) delete[] values;
        if (id) delete[] id;
    }
};

template<typename K, typename V, class RecordManager>
class InterpolationSearchTree {
private:
    const int LEAF_SIZE = 12;
    V noValue;
    Node<K, V>* root;
public:
    InterpolationSearchTree(const V noValue, const K minKey, const K maxKey);

    ~InterpolationSearchTree();

    V find(const int tid, const K &key);

    bool contains(const int tid, const K &key);

    V insertIfAbsent(const int tid, const K &key, const V &value);

    V erase(const int tid, const K &key);

    bool validate();

    void printDebuggingDetails();

    void initThread(const int tid);

    void deinitThread(const int tid);
    
    Node<K, V>* getRoot();

private:
    void updateNode(const int tid, Node<K, V> * nodeToUpdate, const int childNumToUpdate);
    Node<K, V>* buildIdealTree(const int tid, int left, int right, K minKey, K maxKey, vector<NodeData<K, V>> const &data);
};

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::buildIdealTree(const int tid, 
    int l, int r, K minKey, K maxKey,
    vector<NodeData<K, V>> const &data) {
    Node<K, V>* root = new Node<K, V>(minKey, maxKey, int(r - l));
    int len = r - l;
    if (len <= LEAF_SIZE) {
        Node<K, V>* root = new Node<K, V>(minKey, maxKey, len, len);    
        for (int i = 0; i < len; i++) {
            root->values[i + 1] = data[l + i];
        }
        
        root->countIds();
        return root;
    }
    int cl = l;
    int cnt, pos;
    auto blockSize = (root->blockSize);
    for (pos = l + blockSize - 1, cnt = 0; pos < r; cnt++, pos += blockSize) {
        if (cl < pos)
           (root->children[cnt]) = buildIdealTree(tid, cl, pos, root->values[cnt].key, data[pos].key, data);
        cl = pos + 1;
        (root->values[cnt + 1]) = data[pos];
    }
    if (cl < r) {
        (root->children[cnt]) = buildIdealTree(tid, cl, r, root->values[cnt].key, maxKey, data);
    }
    root->countIds();
    return root;
}

template <typename K, typename V>
void Node<K, V>::countIds() {
    int cl = 0;
    for (int i = 0; i < (idSize); i++) {
        K key = values[0].key + (long long)(values[levelSize + 1].key - values[0].key) * i / idSize;
        while (cl < levelSize + 1 && (values[cl]).key <= key) {
            cl++;
        }
        id[i] = cl - 1;
    }
}

template <typename K, typename V>
void Node<K, V>::printInfo(int level) {
    for (int i = 1; i <= levelSize; i++) {
        if (children[i - 1] != NULL)
            children[i - 1]->printInfo(level + 1);
        // if (marks[i] == false)
        //     std::cout << "(key=" << values[i].key << ",value=" << values[i].value << ",level=" << level << "), ";
        K key = values[0].key + (long long)(values[levelSize + 1].key - values[0].key) * i / (levelSize);
    }    
    if (children[levelSize] != NULL)
        children[levelSize]->printInfo(level + 1);
}


template <typename K, typename V, class RecordManager>
InterpolationSearchTree<K, V, RecordManager>::InterpolationSearchTree(const V noValue, const K minKey, const K maxKey)
    : noValue(noValue)
    , root(new Node<K, V>(minKey, maxKey, 0)) {
    root->countIds();
}

template <typename K, typename V, class RecordManager>
bool InterpolationSearchTree<K, V, RecordManager>::contains(const int tid, const K &key) {
    return find(tid, key) == noValue;
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::find(const int tid, const K &key) {
    Node<K, V> *cur = root;
    int pos = 0;
    while (true) {
        pos = cur->findOnLevel(key);
    
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                return (cur->values[pos]).value;
            }
            return noValue;
        }
        if ((cur->children[pos]) == NULL) {
            return noValue;
        }
        cur = (cur->children[pos]);
    }
}


template <typename K, typename V>
inline int Node<K, V>::findOnLevel(const K & key) {
    K minKey = values[0].key;
    K maxKey = values[levelSize + 1].key;
    if (minKey == maxKey) return 0;
    int posApprox = id[(long long)idSize * (key - minKey) / (maxKey - minKey)];
//    std::cout << minKey << " " << maxKey << " " << key << " " << levelSize << " " << posApprox << std::endl;
    int kp = 1;
    while (values[posApprox + 1].key <= key && kp <= levelSize + 2) {
        kp *= 2;
        posApprox++;
    }
    if (kp > levelSize + 2) {
        int l = 0, r = levelSize + 2;
        while (r - l > 1) {
            int mid = (l + r) / 2;
            if (values[mid].key == key)
                return mid;
            if (values[mid].key < key) {
                l = mid;
            } else
                r = mid;
        }
        return l;
    } else
        return posApprox;
}

template <typename K, typename V>
void Node<K, V>::getTreeData(std::vector<NodeData<K, V>> & v) {
    for (int i = 0; i <= levelSize; i++) {
        if (i > 0 && !marks[i]) v.push_back(values[i]);
        if (children[i] != NULL) children[i]->getTreeData(v);
    }
}

namespace {
    template <typename K, typename V>
    void delete_tree(Node<K, V>* node) {
        if (node->children) {
            for (int i = 0; i <= node->levelSize; i++)
                if (node->children[i]) delete_tree(node->children[i]);
        }
        delete node;
    }
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::updateNode(const int tid, Node<K, V> *nodeToUpdate, const int childNumToUpdate) {
    auto nd = nodeToUpdate->children[childNumToUpdate];
    vector<NodeData<K, V>> vec(0);
    nd->getTreeData(vec);
    nodeToUpdate->children[childNumToUpdate] = buildIdealTree(tid, 0,int(vec.size()), nd->values[0].key, nd->values[(nd->levelSize) + 1].key, vec);
    delete_tree(nd);
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::insertIfAbsent(const int tid, const K &key, const V &value) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    int pos = 0;
    Node<K, V> *nodeToUpdate = NULL;
    int childNumToUpdate = 0;
    V result = noValue;

    while (true) {
        cur->numUpdates++; // ? should I update this value in case of unsuccessful insert
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }       
        pos = cur->findOnLevel(key);
        //std::cout << "pos = " << pos << "\n";
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                result = (cur->values[pos]).value;
            } else {
                cur->marks[pos] = false;
                (cur->values[pos]).value = value;
            }
            break;
        }
        //std::cout << key << " " << (cur->values[pos]).key << "\n";
        if ((cur->children[pos]) == NULL) {
             auto nd = new Node<K, V>((cur->values[pos]).key, 
                (cur->values[pos + 1]).key, 1);
             nd->values[1].value = value;
             nd->values[1].key = key;
             nd->countIds();
             cur->children[pos] = nd;
            break;
        }
        prev = cur;
        cur = (cur->children[pos]);
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);
        //std::cout << "should update!\n";
    }
    return result;
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::erase(const int tid, const K &key) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    int pos = 0;
    Node<K, V> *nodeToUpdate = NULL;
    int childNumToUpdate = 0;
    V result = noValue;

    while (true) {
        cur->numUpdates++; // ? should I update this value in case of unsuccessful insert
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }       
        pos = cur->findOnLevel(key);
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                result = (cur->values[pos]).value;
                cur->marks[pos] = true;
            }
            break;
        }
        if ((cur->children[pos]) == NULL) {
            break;
        }
        prev = cur;
        cur = (cur->children[pos]);
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);
    }

    return result;
}

template <typename K, typename V, class RecordManager>
bool InterpolationSearchTree<K, V, RecordManager>::validate() {
    return true;
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::initThread(const int tid) {
//    if (init[tid]) return;
//    else init[tid] = !init[tid];
//    recordManager->initThread(tid);
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::printDebuggingDetails() {
    root->printInfo(0);
    return;
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::deinitThread(const int tid) {
//    if (!init[tid]) return;
//    else init[tid] = !init[tid];
//    recordManager->deinitThread(tid);
}

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::getRoot() {
    return root;
}

template <typename K, typename V, class RecordManager>
InterpolationSearchTree<K, V, RecordManager>::~InterpolationSearchTree() {
}
