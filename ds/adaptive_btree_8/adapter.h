#pragma once

#include <iostream>
#include <csignal>
#include <bits/stdc++.h>
using namespace std;

#include "errors.h"
#include "record_manager.h"
#ifdef USE_TREE_STATS
#   include "tree_stats.h"
#endif
#include "adaptive_btree.h"

#ifndef B_TREE_FACTOR
#define B_TREE_FACTOR 8
#endif

#define RECORD_MANAGER_T record_manager<Reclaim, Alloc, Pool, Node<K, V>>
#define DATA_STRUCTURE_T AdaptiveBTree<K, V, B_TREE_FACTOR>


template <typename K, typename V, class Reclaim = reclaimer_debra<K>, class Alloc = allocator_new<K>, class Pool = pool_none<K>>
class ds_adapter {
private:
    const V NO_VALUE;
    DATA_STRUCTURE_T * const ds;

public:
    ds_adapter(const int NUM_THREADS,
               const K& KEY_MIN,
               const K& KEY_MAX,
               const V& VALUE_RESERVED,
               Random64 * const unused2)
            : NO_VALUE(VALUE_RESERVED)
            , ds(new DATA_STRUCTURE_T(VALUE_RESERVED))
    { }

    ~ds_adapter() {
        delete ds;
    }

    V getNoValue() {
        return 0;
    }

    void initThread(const int tid) {
//        ds->initThread(tid);
    }
    void deinitThread(const int tid) {
//        ds->deinitThread(tid);
    }

    void warmupEnd() {
    }

    V insert(const int tid, const K& key, const V& val) {
        setbench_error("insert-replace functionality not implemented for this data structure");
    }

    V insertIfAbsent(const int tid, const K& key, const V& val) {
        return ds->insertIfAbsent(key, val);
    }

    V erase(const int tid, const K& key) {
        return ds->erase(key);
    }

    V find(const int tid, const K& key) {
        return ds->find(key);
    }

    bool contains(const int tid, const K& key) {
        return ds->contains(key);
    }
    int rangeQuery(const int tid, const K& lo, const K& hi, K * const resultKeys, V * const resultValues) {
        setbench_error("not implemented");
    }
    void printSummary() {
//        ds->printDebuggingDetails();
//        ds->print_inner_structure();
    }
    bool validateStructure() {
//        return ds->validate();
        return true;
    }

    void printObjectSizes() {
        std::cout<<"sizes: node="
                 <<(sizeof(Node<K, V, 16>))
                 <<std::endl;
    }

    std::string getName() {
        return "adaptive_b_tree_" + std::to_string(B_TREE_FACTOR);
    }

//#ifdef USE_TREE_STATS
//    class NodeHandler {
//    public:
//        typedef Node<K, V> * NodePtrType;
//        K minKey;
//        K maxKey;
//
//        NodeHandler(const K& _minKey, const K& _maxKey) {
//            minKey = _minKey;
//            maxKey = _maxKey;
//        }
//
//        class ChildIterator {
//        private:
//            NodePtrType node; // node being iterated over
//            int pos = 0;
//        public:
//            ChildIterator(NodePtrType _node) {
//                node = _node;
//                pos = 0;
//                while (pos < (node->levelSize) + 1 && (node->children[pos] == NULL)) {
//                    pos++;
//                }
//            }
//
//            bool hasNext() {
//                while (pos < (node->levelSize) + 1 && (node->children[pos] == NULL)) {
//                    pos++;
//                }
//                return pos < (node->levelSize) + 1;
//            }
//
//            NodePtrType next() {
//                return node->children[pos];
//            }
//        };
//
//        size_t getNumChildren(NodePtrType node) {
//            auto it = ChildIterator(node);
//            size_t cnt = 0;
//            while ((it.hasNext())) {
//                cnt++;
//                it.next();
//            }
//            return cnt;
//        }
//
//        bool isLeaf(NodePtrType node) {
//            return getNumChildren(node) != 0;
//        }
//
//        size_t getNumKeys(NodePtrType node) {
//            return node == NULL ? 0 : (node->levelSize);
//        }
//
//        size_t getSumOfKeys(NodePtrType node) {
//            return (size_t) 0;
//        }
//        ChildIterator getChildIterator(NodePtrType node) {
//            return ChildIterator(node);
//        }
//    };
//    TreeStats<NodeHandler> * createTreeStats(const K& _minKey, const K& _maxKey) {
//        return new TreeStats<NodeHandler>(new NodeHandler(_minKey, _maxKey), ds->getRoot(), false);
//    }
//#endif
};

