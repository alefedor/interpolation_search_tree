#include "locks_impl.h"
#include <bits/stdc++.h>
using namespace std;
#ifndef LOCK_BASED_IST_H
#define LOCK_BASED_IST_H
const int MAX_LEVEL = 44;
const int MAX_THREADS = 1000;
const int PADDING_SIZE = 128;

template<typename K, typename V>
struct NodeData
{
    K key;
    V value;
};

template<typename K, typename V>
struct Node {
    Node() = delete;
    Node(K _minKey, K _maxKey, int _initSize, int _levelSize)
    : initSize(2 * _initSize)
    , maxUpdates(_initSize)
    , levelSize(_levelSize) {
        children = new Node<K, V>* [levelSize + 1];
        marks = new bool[levelSize + 2];
        accesses = new int[levelSize + 2];
        values = new NodeData<K, V> [levelSize + 2];
        id = new int[levelSize];
        for (int i = 0; i < levelSize + 1; i++) {
            children[i] = NULL;
        }
        for (int i = 0; i < levelSize + 2; i++) {
            marks[i] = false;
            accesses[i] = 0;
            // TODO: use the same value for accesses and mark (negative when deleted)
        }
        for (int i = 0; i < levelSize; i++) {
            id[i] = -1;
        }
        values[0].key = _minKey;
        values[levelSize + 1].key = _maxKey;
    }
    const int initSize = 1, maxUpdates = 1, levelSize = 1;
    int numUpdates = 0;
    NodeData<K, V>* values = NULL;
    bool* marks = NULL;
    Node<K, V>** children = NULL;
    int* id = NULL;
    int* accesses = NULL;
    int findOnLevel(const K & key);
    void getTreeData(int threshold, std::vector<NodeData<K, V> > & v, std::vector<int> & a,
                     std::vector<int> & big);
    void countIds();
};

template<typename K, typename V, class RecordManager>
class InterpolationSearchTree {
private:
    const int MULT = 3, LEAF_SIZE = 5;
    int sumAccesses = 0;
    int numValues = 0;
    RecordManager * const recordManager;
    V noValue;
    Node<K, V>* root;
    int init[MAX_THREADS] = {0,};
public:
    InterpolationSearchTree(const int numThreads, const V noValue, const K minKey, const K maxKey);

    ~InterpolationSearchTree();

    V find(const int tid, const K &key);

    bool contains(const int tid, const K &key);

    V insertIfAbsent(const int tid, const K &key, const V &value);

    V erase(const int tid, const K &key);

    bool validate();

    void printDebuggingDetails();

    void initThread(const int tid);

    void deinitThread(const int tid);
    
    Node<K, V>* getRoot();

private:
    void updateNode(const int tid, Node<K, V> * nodeToUpdate, const int childNumToUpdate);
    Node<K, V>* buildIdealTree(const int tid, int left, int right, K minKey, K maxKey,
    vector<NodeData<K, V>> const &data, 
    vector<int> const &a, 
    vector<int> const &big, 
    vector<int> const &firstToRight);
    Node<K, V>* buildIdealTreeFast(const int tid, int left, int right, K minKey, K maxKey,
    vector<NodeData<K, V>> const &data, 
    vector<int> const &a);
};

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::buildIdealTree(const int tid, 
    int l, int r, K minKey, K maxKey,
    vector<NodeData<K, V>> const &data,
    vector<int> const &accesses,
    vector<int> const &big, 
    vector<int> const &firstToRight) {
    int len = r - l;
    if (len <= LEAF_SIZE) {
        Node<K, V>* root = new Node<K, V>(minKey, maxKey, len, len);    
        for (int i = 0; i < len; i++) {
            root->values[i + 1] = data[l + i];
            root->accesses[i + 1] = accesses[l + i];
        }
        
        root->countIds();
        return root;
    }
    int other_l = firstToRight[l];
    int other_r = firstToRight[r - 1];
    if (other_l == other_r) {
        return buildIdealTreeFast(tid, l, r, minKey, maxKey, data, accesses);
    }

    int curSize = 0;
    int blockSize = (int(sqrt(double(len))) + 1) / 2;
    int levelSize = len / blockSize;
    
    int other_len = (other_r - other_l + levelSize - 1) / levelSize;
    int cur_pos = blockSize - 1;
    int other_pos = other_len - 1;
    int cnt = levelSize;
    while (other_pos < other_r - other_l) {
        if ((big[other_pos + other_l] - l) % blockSize != blockSize - 1)
            cnt++;
        other_pos += other_len;
    }
    Node<K, V>* root = new Node<K, V>(minKey, maxKey, len, cnt);
    other_pos = other_len - 1;
    int cq = 1;
    while (other_pos < other_r - other_l) {
        while (big[other_pos + other_l] - l >= cur_pos) {
            root->accesses[cq] = cur_pos + l;
            cq++;
            cur_pos += blockSize;
        }
        if ((big[other_pos + other_l] - l) % blockSize != blockSize - 1) {
            root->accesses[cq] = big[other_pos + other_l];
            cq++;
        }
        other_pos += other_len;
    }
    while(cur_pos < r - l) {
        root->accesses[cq] = cur_pos + l;
        cq++;
        cur_pos += blockSize;
    }
    int lst_pos = l;
    for (int i = 1; i <= cnt; i++) {
        int cposs = root->accesses[i];
        root->accesses[i] = accesses[cposs];
        root->values[i] = data[cposs];
        if (lst_pos != cposs)
            root->children[i - 1] = buildIdealTree(tid, lst_pos, cposs, root->values[i - 1].key, data[cposs].key, data, accesses, big, firstToRight);
        lst_pos = cposs + 1;
    }
    if (lst_pos != r) {
        root->children[cnt] = buildIdealTree(tid, lst_pos, r, root->values[cnt].key, maxKey, data, accesses, big, firstToRight);
    }
    root->countIds();
    return root;
}

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::buildIdealTreeFast(const int tid, 
    int l, int r, K minKey, K maxKey,
    vector<NodeData<K, V>> const &data,
    vector<int> const &accesses) {
    int len = r - l;
    if (len <= LEAF_SIZE) {
        Node<K, V>* root = new Node<K, V>(minKey, maxKey, len, len);    
        for (int i = 0; i < len; i++) {
            root->values[i + 1] = data[l + i];
            root->accesses[i + 1] = accesses[l + i];
        }
        
        root->countIds();
        return root;
    }
    int cl = l;
    int cnt, pos;
    int blockSize = int(sqrt(double(len)));
    int levelSize = len / blockSize;
    Node<K, V>* root = new Node<K, V>(minKey, maxKey, int(r - l), levelSize);
    
    for (pos = l + blockSize - 1, cnt = 0; pos < r; cnt++, pos += blockSize) {
        if (cl < pos)
           (root->children[cnt]) = buildIdealTreeFast(tid, cl, pos, root->values[cnt].key, data[pos].key, data, accesses);
        cl = pos + 1;
        (root->values[cnt + 1]) = data[pos];
        (root->accesses[cnt + 1]) = accesses[pos];
    }
    if (cl < r) {
        (root->children[cnt]) = buildIdealTreeFast(tid, cl, r, root->values[cnt].key, maxKey, data, accesses);
    }
    root->countIds();
    return root;
}


template <typename K, typename V, class RecordManager>
InterpolationSearchTree<K, V, RecordManager>::InterpolationSearchTree(const int numThreads, const V noValue, const K minKey, const K maxKey)
    : recordManager(new RecordManager(numThreads))
    , noValue(noValue) 
    , root(new Node<K, V>(minKey, maxKey, 0, 0)) {
    const int tid = 0;
    initThread(tid);
}

template <typename K, typename V, class RecordManager>
bool InterpolationSearchTree<K, V, RecordManager>::contains(const int tid, const K &key) {
    return find(tid, key) == noValue;
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::find(const int tid, const K &key) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    Node<K, V> *nodeToUpdate = NULL;
    int pos = 0;
    V result = noValue;
    int childNumToUpdate = 0;

    while (true) {
        cur->numUpdates++;
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }
        pos = cur->findOnLevel(key);
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                cur->accesses[pos]++;
                sumAccesses++;
                result = (cur->values[pos]).value;
            }
            break;
        }
        if ((cur->children[pos]) == NULL) {
            break;
        }
        prev = cur;
        cur = (cur->children[pos]);
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);    
    }
    return result;
}

template <typename K, typename V>
void Node<K, V>::countIds() {
    int cl = 0;
    for (int i = 0; i < (levelSize); i++) {
        K key = values[0].key + (long long)(values[levelSize + 1].key - values[0].key) * i / (levelSize);
        while ((values[cl]).key <= key) {
            cl++;
        }
        id[i] = cl - 1;
    }    
}

template <typename K, typename V>
int Node<K, V>::findOnLevel(const K & key) {
    int posApprox = id[(long long)levelSize * (key - values[0].key) / (values[levelSize + 1].key - values[0].key)];
    int kp = 1;
    while (values[posApprox + 1].key <= key && kp <= levelSize + 2) {
        kp *= 2;
        posApprox++;
    }
    if (kp > levelSize + 2) {
        int l = 0, r = levelSize + 2;
        while (r - l > 1) {
            int mid = (l + r) / 2;
            if (values[mid].key == key)
                return mid;
            if (values[mid].key < key) {
                l = mid;
            } else
                r = mid;
        }
        return l;
    } else
        return posApprox;
}

template <typename K, typename V>
void Node<K, V>::getTreeData(int threshold, std::vector<NodeData<K, V>> & data, 
                             std::vector<int> & a,
                             std::vector<int> & big) {
    for (int i = 0; i <= levelSize; i++) {
        if (i > 0 && marks[i] == false) {
            if (accesses[i] > threshold)
                big.push_back(int(data.size()));
            data.push_back(values[i]); 
            a.push_back(accesses[i]);
        }
        if (children[i] != NULL) children[i]->getTreeData(threshold, data, a, big);
    }
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::updateNode(const int tid, Node<K, V> *nodeToUpdate, const int childNumToUpdate) {
    auto nd = nodeToUpdate->children[childNumToUpdate];
    vector<NodeData<K, V>> vec(0);
    vector<int> big, a;
    int thr = numValues == 0 ? 0 : (long long)sumAccesses * MULT / numValues;
    nd->getTreeData(thr, vec, a, big);
    if (int(big.size()) == 0) {
        nodeToUpdate->children[childNumToUpdate] = buildIdealTreeFast(tid, 0, 
    int(vec.size()), nd->values[0].key, nd->values[(nd->levelSize) + 1].key, vec, a);
        return;
    }
    vector<int> firstToRight(int(vec.size()));
    int cur_pos = 0;
    for (int i = 0; i < int(vec.size()); i++) {
        while (cur_pos < int(big.size()) && big[cur_pos] < i) {
            cur_pos++;
        }
        firstToRight[i] = cur_pos;
    }
    nodeToUpdate->children[childNumToUpdate] = buildIdealTree(tid, 0, 
    int(vec.size()), nd->values[0].key, nd->values[(nd->levelSize) + 1].key, vec, a, big, firstToRight);
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::insertIfAbsent(const int tid, const K &key, const V &value) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    int pos = 0;
    Node<K, V> *nodeToUpdate = NULL;
    int childNumToUpdate = 0;
    V result = noValue;

    while (true) {
        cur->numUpdates += 4;
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }       
        pos = cur->findOnLevel(key);
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                result = (cur->values[pos]).value;
            } else {
                cur->marks[pos] = false;
                (cur->values[pos]).value = value;
                cur->accesses[pos] = 0;
                numValues++;
            }
            break;
        }
        if ((cur->children[pos]) == NULL) {
            auto nd = new Node<K, V>((cur->values[pos]).key, 
            (cur->values[pos + 1]).key, 1, 1);
            nd->values[1].value = value;
            nd->values[1].key = key;
            nd->accesses[1] = 0;
            numValues++;
            nd->countIds();
            cur->children[pos] = nd;
            break;
        }
        prev = cur;
        cur = (cur->children[pos]);
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);    
    }
    return result;
}

template <typename K, typename V, class RecordManager>
V InterpolationSearchTree<K, V, RecordManager>::erase(const int tid, const K &key) {
    Node<K, V> *cur = root;
    Node<K, V> *prev = root;
    int pos = 0;
    Node<K, V> *nodeToUpdate = NULL;
    int childNumToUpdate = 0;
    V result = noValue;

    while (true) {
        cur->numUpdates += 4;
        if (cur != root && (cur->numUpdates) > (cur->maxUpdates) && (nodeToUpdate == NULL)) {
            nodeToUpdate = prev;
            childNumToUpdate = pos;
        }       
        pos = cur->findOnLevel(key);
        if (pos > 0 && (cur->values[pos]).key == key) {
            if ((cur->marks[pos]) == false) {
                result = (cur->values[pos]).value;
                cur->marks[pos] = true;
                sumAccesses -= cur->accesses[pos];
                cur->accesses[pos] = 0;
                numValues--;
            }
            break;
        }
        if ((cur->children[pos]) == NULL) {
            break;
        }
        prev = cur;
        cur = (cur->children[pos]);
    }
    if (nodeToUpdate != NULL) {
        updateNode(tid, nodeToUpdate, childNumToUpdate);    
    }
    return result;
}

template <typename K, typename V, class RecordManager>
bool InterpolationSearchTree<K, V, RecordManager>::validate() {
    //TODO
    return true;
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::initThread(const int tid) {
    if (init[tid]) return;
    else init[tid] = !init[tid];
    recordManager->initThread(tid);
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::printDebuggingDetails() {
    //TODO
    return;
}

template <typename K, typename V, class RecordManager>
void InterpolationSearchTree<K, V, RecordManager>::deinitThread(const int tid) {
    if (!init[tid]) return;
    else init[tid] = !init[tid];
    recordManager->deinitThread(tid);
}

template <typename K, typename V, class RecordManager>
Node<K, V>* InterpolationSearchTree<K, V, RecordManager>::getRoot() {
    return root;
}

template <typename K, typename V, class RecordManager>
InterpolationSearchTree<K, V, RecordManager>::~InterpolationSearchTree() {
    recordManager->printStatus();
    delete recordManager;
}
#endif //LOCK_BASED_IST_H
