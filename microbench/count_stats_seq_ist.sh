make bnch

seconds=(10) 
prefill=5000000
yx=(1 5 10 50)
thr=(1)

#for((cnt=0;cnt<10;cnt++))
#do
for s in "${seconds[@]}"
do
    for threads in "${thr[@]}"
    do
        echo "starting to count zipf, threads=$threads, secs=$s"
        LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_b_tree.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s zipf 1 threads $threads >> results/results.csv
        LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_adaptive_btree.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s zipf 1 threads $threads >> results/results.csv
        LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_ist_sequential_ext.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s zipf 1 threads $threads >> results/results.csv
        LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_ist_sequential.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s zipf 1 threads $threads >> results/results.csv
        LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_ist_seq_weights.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s zipf 1 threads $threads >> results/results.csv
        for y in "${yx[@]}"
        do
            x=$((100-y))
            echo "starting to count x=$x, y=$y, threads=$threads, secs=$s"
            LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_b_tree.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s x $x y $y threads $threads >> results/results.csv
            LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_adaptive_btree.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s x $x y $y threads $threads >> results/results.csv
            LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_ist_sequential_ext.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s x $x y $y threads $threads >> results/results.csv
            LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_ist_sequential.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s x $x y $y threads $threads >> results/results.csv
            LD_PRELOAD=../lib/libjemalloc.so ./bin/bench_ist_seq_weights.alloc_new.reclaim_none.pool_none.out prefill $prefill presecs 0 secs $s x $x y $y threads $threads >> results/results.csv
        done
    done
done
#done
# ./bin/ubench_ist_sequential_ext.alloc_new.reclaim_none.pool_none.out prefill 1000000 presecs 0 secs 10 x 10 y 90 threads 1