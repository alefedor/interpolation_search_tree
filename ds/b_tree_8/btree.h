#pragma once

#include <bits/stdc++.h>

namespace {
    const int MAX_LEVEL = 10;
}

template<typename K, typename V, int B>
struct Node {
    Node() {
        std::memset(children, 0, sizeof(children));
    };

    Node(K* keys, V* values, Node<K, V, B>** children, int size) {
        initialize(keys, values, children, size);
    }

    void initialize(K* keys, V* values, Node<K, V, B>** children, int size) {
        std::memset(this->children, 0, sizeof(this->children));
        std::copy(keys, keys + size, this->keys);
        std::copy(values, values + size, this->values);
        std::copy(children, children + (size + 1), this->children);
        this->size = size;
    }

    int size = 0;

    K keys[2 * B];
    Node* children[2 * B + 1];
    V values[2 * B];
    static const int MAX_KEYS = 2 * B;

    int find_in_node(K const& key) { // return min pos : keys[pos] >= key
        int pos;
        for (pos = 0; pos < size; pos++)
            if (keys[pos] >= key) return pos;
        return pos;
    }
};


template <typename K, typename V, int B>
class BTree {
    typedef Node<K, V, B> BTreeNode;

 private:
    V noValue;
    BTreeNode* root;
 public:

    BTree(V noValue) : noValue(noValue), root(new BTreeNode()) {}

    V find(K const& key) {
        BTreeNode* node = root;
        while (node != nullptr) {
            int pos = node->find_in_node(key);
            if (pos != node->size && node->keys[pos] == key)
                return node->values[pos];
            node = node->children[pos];
        }
        return noValue;
    }

    bool contains(K const& key) {
        return find(key) != noValue;
    }

    V insertIfAbsent(K key, V value) {
        BTreeNode* node = root;
        BTreeNode* nodes[MAX_LEVEL];
        int positions[MAX_LEVEL];

        int level = 0;

        while (true) {
            int pos = node->find_in_node(key);
            nodes[level] = node;
            positions[level] = pos;
            level++;
            if (pos != node->size && node->keys[pos] == key)
                return node->values[pos];

            if (!node->children[pos]) {
                break; // will be inserted here later
            }

            node = node->children[pos];
        }

        BTreeNode * next_node = nullptr;
        for (int l = level - 1; l >= 0; l--) {
            BTreeNode * node = nodes[l];
            int position = positions[l];
            if (node->size == BTreeNode::MAX_KEYS) {
                // needs splitting
                K keys[2 * B + 1];
                V values[2 * B + 1];
                BTreeNode* children[2 * B + 2];
                std::copy(node->keys, node->keys + position, keys);
                std::copy(node->values, node->values + position, values);
                std::copy(node->children, node->children + position, children);



                keys[position] = key;
                values[position] = value;
                children[position] = next_node;

                std::copy(node->keys + position, node->keys + node->size, keys + position + 1);
                std::copy(node->values + position, node->values + node->size, values + position + 1);
                std::copy(node->children + position, node->children + node->size + 1, children + position + 1);

                BTreeNode* new_node = new BTreeNode(&(keys[0]), &(values[0]), &(children[0]), B);
                node->initialize(&(keys[0]) + B + 1, &(values[0]) + B + 1, &(children[0]) + B + 1, B);

                key = keys[B];
                value = values[B];
                next_node = new_node;
            } else {
                for (int i = node->size - 1; i >= position; i--) {
                    node->keys[i + 1] = node->keys[i];
                    node->values[i + 1] = node->values[i];
                }
                for (int i = node->size; i >= position; i--) {
                    node->children[i + 1] = node->children[i];
                }
                node->keys[position] = key;
                node->values[position] = value;
                node->children[position] = next_node;
                node->size++;
                return noValue;
            }
        }

        // The tree "grows" up. Create a new root
        assert(next_node != nullptr);
        BTreeNode* new_root = new BTreeNode();
        new_root->size = 1;
        new_root->keys[0] = key;
        new_root->values[0] = value;
        new_root->children[0] = next_node;
        new_root->children[1] = nodes[0];
        root = new_root;

        return noValue;
    }

    inline void merge_children(BTreeNode* node, int position) {
        // merges node->children[position] and node->children[position+1]

        BTreeNode *left_child = node->children[position];
        BTreeNode *right_child = node->children[position + 1];

        int left_size = left_child->size;
        int right_size = right_child->size;

        assert(left_size + right_size + 1 == 2 * B);

        left_child->keys[left_size] = node->keys[position];
        left_child->values[left_size] = node->values[position];

        node->size--;
        for (int i = position; i < node->size; i++) {
            node->keys[i] = node->keys[i + 1];
            node->values[i] = node->values[i + 1];
        }
        for (int i = position + 1; i <= node->size; i++) {
            node->children[i] = node->children[i + 1];
        }

        std::copy(right_child->keys, right_child->keys + right_size, left_child->keys + (left_size + 1));
        std::copy(right_child->values, right_child->values + right_size, left_child->values + (left_size + 1));
        std::copy(right_child->children, right_child->children + (right_size + 1), left_child->children + (left_size + 1));
        left_child->size = left_size + right_size + 1;

        delete right_child;
    }

    V erase(K const& key) {
        BTreeNode* node = root;
        BTreeNode* nodes[MAX_LEVEL];
        int positions[MAX_LEVEL];

        int level = 0;

        while (node != nullptr) {
            int pos = node->find_in_node(key);
            nodes[level] = node;
            positions[level] = pos;
            level++;
            if (pos != node->size && node->keys[pos] == key) {
                V result = node->values[pos];
                BTreeNode *remove_node = node;
                int remove_pos = pos;

                if (node->children[pos]) {
                    nodes[level] = node->children[pos];
                    positions[level] = node->children[pos]->size;
                    level++;
                    node = node->children[pos];
                    pos = node->size - 1; // the last key

                    while (node->children[node->size]) {
                        nodes[level] = node->children[node->size];
                        positions[level] = node->children[node->size]->size;
                        level++;
                        node = node->children[node->size];
                        pos = node->size - 1; // the last key
                    }

                    // copy a key-value pair into the deleted slot
                    remove_node->keys[remove_pos] = node->keys[pos];
                    remove_node->values[remove_pos] = node->values[pos];
                }

                for (int i = pos; i + 1 < node->size; i++) {
                    node->keys[i] = node->keys[i + 1];
                    node->values[i] = node->values[i + 1];
                    // no need to do something with children since we are in a leaf
                }
                node->size--;

                // Check node size
                if (node->size >= B) return result;

                // Need to increase size back
                for (int l = level - 2; l >= 0; l--) {
                    BTreeNode *node = nodes[l];
                    int position = positions[l];
                    BTreeNode* child = nodes[l + 1];

                    if (child->size < B) {
                        assert(child->size == B - 1);
                        if (position != 0 && node->children[position - 1]->size > B) {
                            node->children[position - 1]->size--;

                            for (int i = child->size - 1; i >= 0; i--) {
                                child->keys[i + 1] = child->keys[i];
                                child->values[i + 1] = child->values[i];
                            }

                            for (int i = child->size; i >= 0; i--) {
                                child->children[i + 1] = child->children[i];
                            }

                            child->keys[0] = node->keys[position - 1];
                            child->values[0] = node->values[position - 1];
                            child->children[0] = node->children[position - 1]->children[node->children[position - 1]->size + 1];

                            node->keys[position - 1] = node->children[position - 1]->keys[node->children[position - 1]->size];
                            node->values[position - 1] = node->children[position - 1]->values[node->children[position - 1]->size];

                            child->size++;
                            return result;
                        } else if (position != node->size && node->children[position + 1]->size > B) {

                            child->keys[child->size] = node->keys[position];
                            child->values[child->size] = node->values[position];
                            child->children[child->size + 1] = node->children[position + 1]->children[0];

                            node->keys[position] = node->children[position + 1]->keys[0];
                            node->values[position] = node->children[position + 1]->values[0];

                            node->children[position + 1]->size--;
                            child->size++;

                            for (int i = 0; i < node->children[position + 1]->size; i++) {
                                node->children[position + 1]->keys[i] = node->children[position + 1]->keys[i + 1];
                                node->children[position + 1]->values[i] = node->children[position + 1]->values[i + 1];
                            }

                            for (int i = 0; i <= node->children[position + 1]->size; i++) {
                                node->children[position + 1]->children[i] = node->children[position + 1]->children[i + 1];
                            }
                            return result;
                        } else if (position != 0) {
                            merge_children(node, position - 1);
                        } else {
                            merge_children(node, position);
                        }
                    } else {
                        break;
                    }
                }

                if (root->size == 0 && root->children[0]) {
                    BTreeNode *old_root = root;
                    root = root->children[0];
                    delete old_root;
                }

                return result;
            }
            node = node->children[pos];
        }
        return noValue;
    }

    void print_inner_structure() {
        print(root);
    }

    void print(BTreeNode* node) {
        if (!node) return;
        std::cout << node << " * " << node->size << std::endl;
        for (int i = 0; i < node->size; i++)
            std::cout << "(" << node->keys[i] << "," << node->values[i] << "); ";
        std::cout << endl;
        for (int i = 0; i <= node->size; i++)
            std::cout << node->children[i] << ", ";
        std::cout << endl;
        for (int i = 0; i <= node->size; i++)
            print(node->children[i]);
    }
};