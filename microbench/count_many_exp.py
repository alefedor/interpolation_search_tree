
prefix = "./results_si_10sec/"
filenames = ["external", "internal", "internal_weights"]
workloads = ["50", "90", "95", "99", "zipf"]
for wl in workloads:
    print("workload: " + wl)
    for filename in filenames:
        inp = open(prefix + filename + "_" + wl, "r")
        # print(filename)
        sm = 0.0
        cnt = 0
        line = inp.readline()
        for s in line.split(" "):
            if (s == ""):
                continue
            sm = sm + float(int(s))
            cnt = cnt + 1
        # print(line)
        sm /= float(cnt)
        print(str(sm) + " " + filename)
        inp.close()
    print()