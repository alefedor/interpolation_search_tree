//
// Created by demon1999 on 22.11.19.
//
#include <bits/stdc++.h>
#include "globals_extern.h"
#include "random_fnv1a.h"
#include "plaf.h"
#include "binding.h"
#include "papi_util_impl.h"
#include "rq_provider.h"
#include "keygen.h"
#include "adapter.h"
#define VALUE_TYPE int
#define KEY_TYPE int
#define DS_ADAPTER_T ds_adapter<KEY_TYPE, VALUE_TYPE, RECLAIM<>, ALLOC<>, POOL<> >

const int MAX_KEY = 50;
const int STRESS_MAX_THREADS = 3;


Random64 rngs[MAX_THREADS_POW2];
using namespace std;

const int NO_VALUE = 0;

DS_ADAPTER_T* orderedSet = new DS_ADAPTER_T(STRESS_MAX_THREADS, 0, MAX_KEY, NO_VALUE, rngs);

const int OPERATIONS = 40;
const int THREAD_KEYS = 10;


vector<vector<pair<pair<int, int>, int> > > operations;

mutex my_lock;
bool is_ok = true;

casword_t volatile arr = 2;
casword_t volatile arr2 = 2;
dcssProvider<void* /* unused */> * const prov = new dcssProvider<void* /* unused */>(3);

void task(int j) {
    if (j == 0 || j == 2) {
        casword_t prev = arr;
        for (int jj = 4; jj < 100000000; jj+=2) {
            casword_t nw = (((uint64_t)jj) << 32) + jj;
            auto res = prov->dcssPtr(j, (casword_t*)&arr2, (casword_t)2, (casword_t*)&arr, prev, nw);
            // if (res.status != DCSS_SUCCESS) {
            //     std::cout << res.status << "\n";
            //     exit(0);
            // }
            // assert(res.status == DCSS_SUCCESS);
            prev = nw;
            // std::cerr << prev << "\n";
            // if (jj % 10000 == 0) {
            //     std::cout << arr << " tid0\n";
            // }
        }
    } else {
        for (int jj = 2; jj < 100000000; jj++) {
            auto val = prov->readPtr(j, &arr);
            if (jj % 1000000 == 0)
                fprintf(stderr, "ok: %016lx\n", val);
            if ((val >> 32) != (val & ((1ULL << 32) - 1)) && val != 2) {
                fprintf(stderr, "Failed: %016lx\n", val);
                exit(0);
            }

        }
    }
}

int main() {
    std::cout << *((casword_t*)&arr) << "\n";
    auto numThreads = 3;
    thread a[numThreads];
    for (int i = 0; i < numThreads; i++) {
        a[i] = thread(task, i);
    }
    for (int i = 0; i < numThreads; i++) {
        a[i].join();
    }
}

