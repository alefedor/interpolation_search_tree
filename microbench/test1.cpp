//
// Created by demon1999 on 22.11.19.
//
#include <bits/stdc++.h>
#include "globals_extern.h"
#include "random_fnv1a.h"
#include "plaf.h"
#include "binding.h"
#include "papi_util_impl.h"
#include "rq_provider.h"
#include "keygen.h"
#include "adapter.h"
#define VALUE_TYPE int
#define KEY_TYPE int
#define DS_ADAPTER_T ds_adapter<KEY_TYPE, VALUE_TYPE, RECLAIM<>, ALLOC<>, POOL<> >

const int MAX_KEY = 10000;
const int STRESS_MAX_THREADS = 4;


Random64 rngs[MAX_THREADS_POW2];
using namespace std;

const int NO_VALUE = 0;

DS_ADAPTER_T* orderedSet = new DS_ADAPTER_T(STRESS_MAX_THREADS, 0, MAX_KEY, NO_VALUE, rngs);

// const int OPERATIONS = 40;
const int THREAD_KEYS = 300;


vector<vector<pair<pair<int, int>, int> > > operations;

mutex my_lock;
bool is_ok = true;


void task(int j) {
    map<int, int> s;
    size_t pos = 0;
    for (pos = 0; pos < operations[j].size(); pos++) {
        int key = operations[j][pos].first.second;
        int op_type = operations[j][pos].first.first;
        int val =  operations[j][pos].second;
        if (op_type == 0) {
            auto value = orderedSet->find(j, key);
            if (value != NO_VALUE && (s.find(key) == s.end())) {
                my_lock.lock();
                std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << value << "\n";
                is_ok = false;
                my_lock.unlock();
            }
            if ((s.find(key) != s.end()) && value != s[key]) {
                my_lock.lock();
                std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " <<  s[key] << value << "\n";
                is_ok = false;
                my_lock.unlock();
            }
        }
        if (op_type == 1) {
            int value = orderedSet->erase(j, key);
            if ((s.find(key) != s.end()) && value != s[key]) {
                my_lock.lock();
                std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << s[key] << value << "\n";
                is_ok = false;
                my_lock.unlock();
            }
            if (value != NO_VALUE && (s.find(key) == s.end())) {
                my_lock.lock();
                std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << value << "\n";
                is_ok = false;
                my_lock.unlock();
            }
            if (s.find(key) != s.end()) {
                s.erase(key);
            }
        }
        if (op_type == 2) {
            int value = orderedSet->insertIfAbsent(j, key, val);
            if ((s.find(key) == s.end()) && value != NO_VALUE) {
                my_lock.lock();
                std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << value << "\n";
                is_ok = false;
                my_lock.unlock();
            }
            if ((s.find(key) != s.end()) && value != s[key]) {
                my_lock.lock();
                std::cout << "op on thread " << j << ", (" << op_type << ", " << key << ", " << val << ") failed " << s[key] << value << "\n";
                is_ok = false;
                my_lock.unlock();
            }
            if (s.find(key) == s.end())
                s[key] = val;
        }
    }
}

int main(int argc, char **argv) {
    srand(2399);
    // srand(atoi(argv[1]));
    //  std::cout << atoi(argv[1]) << " kek\n";
    int numThreads = rand() % STRESS_MAX_THREADS + 1;
    // cout << "Number of threads is " << numThreads << "\n";
    //exit(0);
    set<long long> ks;
    ks.insert(0);
    vector<long long> keys;
    long long min_key = 0;
    long long max_key = MAX_KEY;
    for (int i = 0; i < numThreads * THREAD_KEYS; i++) {
        long long k = rand() % MAX_KEY;
        while (ks.find(k) != ks.end()) {
            k = rand() % MAX_KEY;
        }
        keys.push_back(k);
        ks.insert(k);
    }
    operations.resize(numThreads, vector<pair<pair<int, int>, int> >(0));
    for (int i = 0; i < OPERATIONS; i++) {
        int op_type = rand() % 3;
        int th = rand() % numThreads;
        int key = keys[(rand() % THREAD_KEYS) + THREAD_KEYS * th];
        operations[th].push_back(make_pair(make_pair(op_type, key), rand() % MAX_KEY + 1));
    }
    cerr << numThreads << "\n";
        for (size_t i = 0; i < operations.size(); i++) {
            cerr << int(operations[i].size()) << "\n";
            int k = 0;
            for (auto op : operations[i]) {
                // k++;
                // if (k > 10) break;
                if (op.first.first == 0)
                    cerr << "0 ";
                if (op.first.first == 1)
                    cerr << "1 ";
                if (op.first.first == 2)
                    cerr << "2 ";
                cerr << op.first.second << " " << op.second << "\n";
            }
        }
    // exit(0);
    for (int iko = 0; iko < 1; iko++) {
        std::cout << "\n\n\n\nITERATION " << iko << "\n";
        orderedSet = new DS_ADAPTER_T(STRESS_MAX_THREADS, 0, MAX_KEY, NO_VALUE, rngs);
        thread a[numThreads];
        for (int i = 0; i < numThreads; i++) {
            a[i] = thread(task, i);
        }
        for (int i = 0; i < numThreads; i++) {
            a[i].join();
        }
        
        if (is_ok) {
            cout << "You are great!!!\n";
        } else {
            // assert(0);
            cout << "Something went wrong!!!\n";
            // exit(0);
            cout << "Number of threads is " << numThreads << "\n";
            for (size_t i = 0; i < operations.size(); i++) {
                cout << "Operations on thread " << i << "\n";
                int k = 0;
                for (auto op : operations[i]) {
                    // k++;
                    // if (k > 10) break;
                    if (op.first.first == 0)
                        cout << "contains ";
                    if (op.first.first == 1)
                        cout << "erase ";
                    if (op.first.first == 2)
                        cout << "insertIfAbsent ";
                    cout << op.first.second << " " << op.second << "\n";
                }
            }
            exit(0);
        }
        orderedSet->validateStructure();
    }
}

