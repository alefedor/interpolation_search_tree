//
// Created by demon1999 on 22.11.19.
//
#include <bits/stdc++.h>
#include "globals_extern.h"
#include "plaf.h"
#include "binding.h"
#include "papi_util_impl.h"
#include "rq_provider.h"
#include "random_fnv1a.h"
#include "keygen.h"
#include "adapter.h"
#define VALUE_TYPE int
#define KEY_TYPE int
#define DS_ADAPTER_T ds_adapter<KEY_TYPE, VALUE_TYPE, RECLAIM<>, ALLOC<>, POOL<> >

using namespace std;

const int NO_VALUE = -1;
const int MAX_KEY = 100;

Random64 rngs[MAX_THREADS_POW2];

namespace {
    int random_int(int min, int max) {
        static thread_local std::mt19937* gen = nullptr;
        if (!gen) gen = new std::mt19937(456457);
        uniform_int_distribution<int> distribution(min, max);
        return distribution(*gen);
    }
}

void test_simple() {
    DS_ADAPTER_T ds(1, 0, MAX_KEY, NO_VALUE, rngs);
    assert(ds.insertIfAbsent(0, 2, 1) == NO_VALUE);
    assert(ds.insertIfAbsent(0, 2, 2) == 1);
    assert(ds.find(0, 2) == 1);
    assert(ds.insertIfAbsent(0, 1, 3) == NO_VALUE);
    assert(ds.find(0, 1) == 3);
    assert(ds.find(0, 2) == 1);
    assert(ds.insertIfAbsent(0, 3, 2) == NO_VALUE);
    assert(ds.erase(0, 0) == NO_VALUE);
    assert(ds.erase(0, 2) == 1);
    assert(ds.find(0, 2) == NO_VALUE);
}

void test_stress() {
    const int iterations = 5000000;

    std::map<int, int> mp;
    DS_ADAPTER_T ds(1, 0, MAX_KEY + 1, NO_VALUE, rngs);

    for (int i = 0; i < iterations; i++) {
        int type = random_int(0, 2);
        int key = random_int(0, MAX_KEY);
        int value = random_int(0, 20);
//        std::cout << type << " " << key << " " << value << std::endl;
//        ds.printSummary();
        if (type == 0) {
            if (mp.count(key)) {
                assert(ds.find(0, key) == mp[key]);
            } else {
                assert(ds.find(0, key) == NO_VALUE);
            }
        } else if (type == 1) {
            if (mp.count(key)) {
                int result = ds.insertIfAbsent(0, key, value);
                int expected = mp[key];
                if (result != expected) {
                    std::cout << result << " != " << expected << std::endl;
                    assert(false && "insert: result != expected");
                }
            } else {
                assert(ds.insertIfAbsent(0, key, value) == NO_VALUE);
                mp[key] = value;
            }
        } else {
            if (mp.count(key)) {
                int result = ds.erase(0, key);
                int expected = mp[key];
                if (result != expected) {
                    std::cout << result << " != " << expected << std::endl;
                    assert(false && "erase: result != expected");
                }
                mp.erase(key);
            } else {
                assert(ds.erase(0, key) == NO_VALUE);
            }
        }
    }
}

int main() {
    test_simple();
    test_stress();
}

