/**
 * -----------------------------------------------------------------------------
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXT_SA_IST_LF_IMPL_H
#define EXT_SA_IST_LF_IMPL_H

#ifdef GSTATS_HANDLE_STATS
#   ifndef __AND
#      define __AND ,
#   endif
#   define GSTATS_HANDLE_STATS_BROWN_EXT_IST_LF(gstats_handle_stat) \
        gstats_handle_stat(LONG_LONG, num_bail_from_addkv_at_depth, 10, { \
                gstats_output_item(PRINT_RAW, SUM, BY_THREAD) \
          __AND gstats_output_item(PRINT_RAW, SUM, BY_INDEX) \
        }) \
        gstats_handle_stat(LONG_LONG, num_bail_from_build_at_depth, 10, { \
                gstats_output_item(PRINT_RAW, SUM, BY_INDEX) \
        }) \
        gstats_handle_stat(LONG_LONG, num_help_subtree, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, num_try_rebuild_at_depth, 100, { \
                gstats_output_item(PRINT_RAW, SUM, BY_INDEX) \
        }) \
        gstats_handle_stat(LONG_LONG, num_complete_rebuild_at_depth, 100, { \
                gstats_output_item(PRINT_RAW, SUM, BY_INDEX) \
        }) \
        gstats_handle_stat(LONG_LONG, num_help_rebuild, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, duration_markAndCount, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, duration_wastedWorkBuilding, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, duration_buildAndReplace, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, duration_rotateAndFree, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \
        gstats_handle_stat(LONG_LONG, duration_traverseAndRetire, 1, { \
                gstats_output_item(PRINT_RAW, SUM, TOTAL) \
        }) \

    // define a variable for each stat above
    GSTATS_HANDLE_STATS_BROWN_EXT_IST_LF(__DECLARE_EXTERN_STAT_ID);
#endif

#ifdef MEASURE_DURATION_STATS
class TimeThisScope {
private:
    bool condition;
    int tid;
    gstats_stat_id stat_id;
    uint64_t start;
public:
    TimeThisScope(const int _tid, gstats_stat_id _stat_id, bool _condition = true) {
        condition = _condition;
        if (condition) {
            tid = _tid;
            stat_id = _stat_id;
            start = get_server_clock();
        }
    }
    ~TimeThisScope() {
        if (condition) {
            auto duration = get_server_clock() - start;
            GSTATS_ADD(tid, stat_id, duration);
        }
    }
};
#endif

#define PREFILL_BUILD_FROM_ARRAY
#define IST_INIT_PARALLEL_IDEAL_BUILD
//#define IST_DISABLE_MULTICOUNTER_AT_ROOT
//#define NO_REBUILDING
//#define PAD_CHANGESUM
//#define IST_DISABLE_COLLABORATIVE_MARK_AND_COUNT
#define MAX_ACCEPTABLE_LEAF_SIZE (120)

#define GV_FLIP_RECORDS

#include <string>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <set>
#include <limits>
#include <vector>
#include <cmath>
#include <unistd.h>
#include <sys/types.h>
#include "random_fnv1a.h"
#ifdef MEASURE_DURATION_STATS
#include "server_clock.h"
#endif
#include "record_manager.h"
#include "dcss_impl.h"
#ifdef _OPENMP
#   include <omp.h>
#endif
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
#   include "multi_counter.h"
#endif

// Note: the following are hacky macros to essentially replace polymorphic types
//       since polymorphic types are unnecessarily expensive. A child pointer in
//       a node can actually represent several different things: a pointer to
//       another node, a pointer to a key-value pair, a pointer to a rebuild
//       object, or a value. To figure out which is the case, use the macros
//       IS_[NODE|KVPAIR|REBUILDOP|VAL]. To cast neutral casword_t types to
//       pointers to these objects, use CASWORD_TO_[NODE|KVPAIR|REBUILDOP|VAL].
//       To cast object pointers to casword_t, use the macros
//       [NODE|KVPAIR|REBUILDOP|VAL]_TO_CASWORD. There is additionally a special
//       reserved/distinguished "EMPTY" value, which can be identified by using
//       IS_EMPTY_VAL. To store an empty value, use EMPTY_VAL_TO_CASWORD.

// for fields Node::ptr(...)
#define casword_t uintptr_t
#define TYPE_MASK                   (0x6ll)
//#define DCSS_BITS                   (1)
//#define TYPE_BITS                   (2)
//#define TOTAL_BITS                  (DCSS_BITS+TYPE_BITS)
#define TOTAL_MASK                  (0x7ll)

#define NODE_MASK                   (0x0ll) /* no need to use this... 0 mask is implicit */
#define IS_NODE(x)                  (((x)&TYPE_MASK)==NODE_MASK)
#define CASWORD_TO_NODE(x)          ((Node<K,V> *) (x))
#define NODE_TO_CASWORD(x)          ((casword_t) (x))
#define IS_EMPTY_CASWORD(x)            (CASWORD_TO_NODE(x) == NULL)
#define CASWORD_TO_VAL(x)           ((V) (x >> 1))
#define VAL_TO_CASWORD(x)           ((casword_t) (x << 1))

#define CASWORD_TO_ACCESS(x)        ((uint64_t) (x >> 1))
#define ACCESS_TO_CASWORD(x)        ((casword_t) (x << 1))
#define VAL_MASK                    (0x6ll)
#define EMPTY_VAL_TO_CASWORD        (((casword_t) ~TOTAL_MASK) | VAL_MASK)



#define REBUILDOP_MASK              (0x4ll)
#define IS_REBUILDOP(x)             (((x)&TYPE_MASK)==REBUILDOP_MASK)
#define CASWORD_TO_REBUILDOP(x)     ((RebuildOperation<K,V> *) ((x)&~TYPE_MASK))
#define REBUILDOP_TO_CASWORD(x)     ((casword_t) (((casword_t) (x))|REBUILDOP_MASK))
#define UNDONE (1 << 16ll)

// for field Node::dirty
// note: dirty finished should imply dirty started!
#define DIRTY_STARTED_MASK          (0x1ll)
#define DIRTY_FINISHED_MASK         (0x2ll)
#define DIRTY_MARKED_FOR_FREE_MASK  (0x4ll) /* used for memory reclamation */
#define IS_DIRTY_STARTED(x)         ((x)&DIRTY_STARTED_MASK)
#define IS_DIRTY_FINISHED(x)        ((x)&DIRTY_FINISHED_MASK)
#define IS_DIRTY_MARKED_FOR_FREE(x) ((x)&DIRTY_MARKED_FOR_FREE_MASK)
#define SUM_TO_DIRTY_FINISHED(x)    (((x)<<3)|DIRTY_FINISHED_MASK|DIRTY_STARTED_MASK)
#define DIRTY_FINISHED_TO_SUM(x)    ((x)>>3)
#define UNSET_VALUE int(1e8)

const double ALPHA = 0.5; //idealness rate

// constants for rebuilding
#define REBUILD_FRACTION            (0.9) /* any subtree will be rebuilt after a number of updates equal to this fraction of its size are performed; example: after 250k updates in a subtree that contained 1M keys at the time it was last rebuilt, it will be rebuilt again */

//static thread_local Random64 * myRNG = NULL; //new Random64(rand());

enum UpdateType {
    InsertIfAbsent, InsertReplace, Erase, Find
};


template <typename K, typename V>
struct Node {
    size_t volatile degree;
    K minKey;
    K maxKey;
    size_t capacity;            // field likely not needed (but convenient and good for debug asserts)
//    size_t initSize;            // initial size (at time of last rebuild) of the subtree rooted at this node
    uint64_t initAc;              // initial sum of accesses
    uint64_t volatile dirty;      // 2-LSBs are marked by markAndCount; also stores the number of pairs in a subtree as recorded by markAndCount (see SUM_TO_DIRTY_FINISHED and DIRTY_FINISHED_TO_SUM)
    size_t volatile nextMarkAndCount; // facilitates recursive-collaborative markAndCount() by allowing threads to dynamically soft-partition subtrees (NOT workstealing/exclusive access---this is still a lock-free mechanism)
    size_t volatile realDegree = 0;
    //TODO: think how not to have two additional values
    size_t volatile nextGetSplitter = 0;
    size_t volatile finishedSplitting = 0;


#ifdef PAD_CHANGESUM
    PAD;
#endif
    volatile uint64_t changeSum;  // could be merged with initSize above (subtract make initSize 1/4 of what it would normally be, then subtract from it instead of incrementing changeSum, and rebuild when it hits zero)
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
    MultiCounter * externalChangeCounter; // NULL for all nodes except the root (or top few nodes), and supercedes changeSum when non-NULL.
#endif
    // unlisted fields: capacity-1 keys of type K followed by capacity values/pointers of type casword_t
    // the values/pointers have tags in their 3 LSBs so that they satisfy either IS_NODE, IS_KVPAIR, IS_REBUILDOP or IS_VAL

    //change sum is something else it depends on number of accesses

    inline casword_t volatile * valAddr(const int ix) {
//        if (degree > capacity) {
//            printf("SOMETHING WENT WRONG VAL ADDR\n");
//            exit(0);
//        }
//        assert(degree <= capacity);
        casword_t * const firstVal = ((casword_t * ) (((char *) this)+sizeof(Node<K,V>)));
        return &firstVal[ix];
    }

    inline casword_t volatile& val(const int ix) {
//        if (ix < 0 || ix >= capacity - 1) {
//            std::cout << "something ww4\n";
//            exit(0);
//        }
//        assert(ix >= 0);
//        if (ix >= degree) {
//            std::cerr << ix << " " << degree << " assert\n";
//            exit(0);
//        }
//        assert(ix < capacity - 1);
        return *(valAddr(ix));
    }

    inline casword_t volatile * accessAddr(const int ix) {
        casword_t * const firstAc = ((casword_t *) (valAddr(capacity - 1)));
        return &firstAc[ix];
    }

    inline casword_t volatile& access(const int ix) {
//        if (ix < 0 || ix >= capacity - 1) {
//            std::cout << "something ww3\n";
//            exit(0);
//        }
//        assert(ix >= 0);
//        assert(ix < capacity - 1);
        return *accessAddr(ix);
    }

    // conceptually returns &node.ptrs[ix]
    inline casword_t volatile * ptrAddr(const int ix) {
//        if (ix < 0) {
//            std::cout << "something ww2\n";
//            exit(0);
//        }
//        assert(ix >= 0);
        casword_t * const firstPtr = (casword_t *) ((char *)accessAddr(capacity - 1));
        return &firstPtr[ix];
    }

    // conceptually returns node.ptrs[ix]
    inline casword_t volatile ptr(const int ix) {
        return *ptrAddr(ix);
    }


    inline K volatile * keyAddr(const int ix) {
        K * const firstKey = ((K *) ((char *) ptrAddr(capacity)));
        return &firstKey[ix];
    }

    inline K volatile& key(const int ix) {
//        if (ix < 0 || ix >= capacity - 1) {
//            printf("ix: %d\n", ix);
//            exit(0);
//        }
//        if (ix < 0 || ix >= capacity - 1) {
//            std::cout << "something ww3\n";
//            exit(0);
//        }
//        assert(ix >= 0);
//        assert(ix < capacity - 1);
        return *keyAddr(ix);
    }

    inline int volatile * idAddr(const int ix) {
        int * const firstId = ((int *) (keyAddr(capacity - 1)));
        return &firstId[ix];
    }

    inline int volatile & id(const int ix) {
//        if (ix < 0 || ix >= capacity) {
//            std::cout << "something ww1\n";
//            exit(0);
//        }
//        assert(ix >= 0);
//        assert(ix < capacity);

        return *idAddr(ix);
    }

    inline int volatile * splittersAddr(const int ix) {
        int volatile * const firstId = (idAddr(capacity));
        return &firstId[ix];
    }

    inline int volatile & splitters(const int ix) {
//        if (ix < 0 || ix >= capacity) {
//            std::cout << "something ww1\n";
//            exit(0);
//        }
//        assert(ix >= 0);
//        assert(ix < capacity);

        return *splittersAddr(ix);
    }


    inline void increaseChangeSum(const int tid, Random64 * rng, uint64_t in) {
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
        if (likely(externalChangeCounter == NULL)) {
            __sync_fetch_and_add(&changeSum, in);
        } else {
            externalChangeCounter->inc(tid, rng, in);
        }
#else
        __sync_fetch_and_add(&changeSum, 1);
#endif
    }
    inline size_t readChangeSum(const int tid, Random64 * rng) {
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
        if (likely(externalChangeCounter == NULL)) {
            return changeSum;
        } else {
            auto res = externalChangeCounter->readFast(tid, rng);;
            return res;
        }
#else
        return changeSum;
#endif
    }

    inline void calcId() {
        int cl = 0;
        for (int i = 0; i < capacity; i++) {
            K k = minKey + (uint64_t)(maxKey - minKey) * i / capacity;
            while (cl < degree - 1 && key(cl) <= k) {
                cl++;
            }
            id(i) = cl;
        }
    }

    inline void calcIdConcurrent() {
        int cl = 0;
        for (int i = 0; i < capacity; i++) {
            K k = minKey + (uint64_t)(maxKey - minKey) * i / capacity;
            while (cl < degree - 1 && key(cl) <= k) {
                cl++;
            }
            __sync_bool_compare_and_swap((idAddr(i)), 0, cl);
        }
    }
};

template <typename K, typename V>
struct RebuildOperation {
    Node<K,V> * rebuildRoot;
    Node<K,V> * parent;
    size_t index;
    size_t depth;
    casword_t volatile newRoot;
    uint64_t volatile success;
    size_t volatile nextIdPos = 0;
    size_t volatile finishedId = 0;
    int volatile debug_sync_in_experimental_no_collaboration_version; // serves as a sort of lock in a crappy version of the algorithm that is only included to show the advantage of our collaborative rebuilding technique (vs this crappy algorithm that has no collaborative rebuilding) ;; 0=unlocked, 1=locked in progress, 2=locked forever done
    RebuildOperation(Node<K,V> * _rebuildRoot, Node<K,V> * _parent, size_t _index, size_t _depth)
        : rebuildRoot(_rebuildRoot), parent(_parent), index(_index), depth(_depth), newRoot(NODE_TO_CASWORD(NULL)), success(0),
        debug_sync_in_experimental_no_collaboration_version(0) {}
};

template <typename K, typename V>
struct KVPair {
    K k;
    V v;
};

template <typename K, typename V>
struct KVPairWithAccesses {
    KVPair<K, V> kv;
    uint64_t ac, prefSumAc;
};

template <typename V>
struct IdealSubtree {
    casword_t ptr;
    V minVal;
};

template <typename K, typename V, class Interpolate, class RecManager>
class istree {
private:
    uint64_t num_int_searches = 0;
    uint64_t sum_steps = 0;
    uint64_t num_paths = 0;
    uint64_t avg_path_length = 0;
    PAD;
    RecManager * const recordmgr;
    dcssProvider<void* /* unused */> * const prov;
    Interpolate cmp;

    Node<K,V> * root;

    uint64_t markAndCount(const int tid, const casword_t ptr, bool tryTiming = true);
    void rebuild(const int tid, Node<K,V> * rebuildRoot, Node<K,V> * parent, int index, const size_t depth);
    void helpRebuild(const int tid, RebuildOperation<K,V> * op);
    int interpolationSearch(const int tid, volatile K key, Node<K,V> * const node);
    V doUpdate(const int tid, const K& key, const V& val, UpdateType t);
    V doFind(const int tid, const K& key);

    Node<K,V> * createNode(const int tid, const uint64_t initAc, const uint64_t degree);
    Node<K,V> * createLeaf(const int tid, KVPairWithAccesses<K,V> * pairs, int numPairs);
    Node<K,V> * createMultiCounterNode(const int tid, const uint64_t initAc, const uint64_t degree);

    void debugPrintWord(std::ofstream& ofs, casword_t w) {
        //ofs<<(void *) (w&~TOTAL_MASK)<<"("<<(IS_REBUILDOP(w) ? "r" : IS_VAL(w) ? "v" : "n")<<")";
        //ofs<<(IS_REBUILDOP(w) ? "RebuildOp *" : IS_VAL(w) ? "V" : "Node *");
        //ofs<<(IS_REBUILDOP(w) ? "r" : IS_VAL(w) ? "v" : "n");
    }

    void debugGVPrint(std::ofstream& ofs, casword_t w, size_t depth, int * numPointers) {
    }
public:
    void printGV(const int tid) {
        std::cerr << tid << ": PRINTING DS:\n";
        std::vector<Node<K, V> *> nodes;
        nodes.push_back(root);
        while (!nodes.empty()) {
            auto cur = nodes.back();
            nodes.pop_back();
            if (cur == NULL) continue;

//            assert(cur->degree <= cur->capacity);
            std::cerr << cur->minKey << ":minkey, ";
            for (int i = 0; i < std::min((size_t )cur->degree, (size_t)cur->capacity); i++){
                if (i + 1 < (cur->degree))
                    std::cerr << "(" << cur->key(i) << ", " << CASWORD_TO_VAL(cur->val(i)) << ", "<< CASWORD_TO_VAL(cur->access(i)) << "), ";
                auto ptr = prov->readPtr(tid, cur->ptrAddr(i));
                while (ptr != NODE_TO_CASWORD(NULL) && IS_REBUILDOP(ptr)) {
                    ptr = NODE_TO_CASWORD(CASWORD_TO_REBUILDOP(ptr)->rebuildRoot);
                }
                nodes.push_back(CASWORD_TO_NODE(ptr));
            }
            std::cerr << cur->maxKey << ":maxkey\n";
        }
    }

    void debugGVPrint(const int tid = 0) {
        printGV(tid);
        std::vector<Node<K, V> *> nodes;
        nodes.push_back(root);
        while (!nodes.empty()) {
            auto cur = nodes.back();
            nodes.pop_back();
            if (cur == NULL) continue;
//            if ((cur->degree > cur->capacity)) {
//                std::cout << "something went wrong19\n";
//                printGV(tid);
//                exit(0);
//            }
//            assert(cur->degree <= cur->capacity);
            for (int i = 0; i < cur->degree; i++){
                auto ptr = prov->readPtr(tid, cur->ptrAddr(i));
                while (ptr != NODE_TO_CASWORD(NULL) && IS_REBUILDOP(ptr)) {
                    ptr = NODE_TO_CASWORD(CASWORD_TO_REBUILDOP(ptr)->rebuildRoot);
                }
                nodes.push_back(CASWORD_TO_NODE(ptr));
                if (i + 1 < cur->degree) {
                    if (i) {
                        if (cur->key(i) < cur->key(i - 1)) {
                            std::cout << "something went wrong15\n";
                            exit(0);
                        }

//                        assert(cur->key(i) >= cur->key(i - 1));
                    } else {
                        if (cur->key(i) < cur->minKey) {
                            std::cout << "something went wrong16\n";
                            exit(0);
                        }
//                        assert(cur->key(i) >= cur->minKey);
                    }
                    if (i + 2 < cur->degree) {
//                        if (cur->key(i) > cur->key(i + 1)) {
//                            exit(0);
//                        }
                        if (cur->key(i) > cur->key(i + 1)) {
                            std::cout << "something went wrong17\n";
                            exit(0);
                        }
                        assert(cur->key(i) <= cur->key(i + 1));
                    } else {
                        if (cur->key(i) > cur->maxKey) {
                            std::cout << "something went wrong18\n";
                            exit(0);
                        }
                        assert(cur->key(i) <= cur->maxKey);
                    }
                }
            }
        }
    }
private:

    void helpFreeSubtree(const int tid, Node<K,V> * node);

    void freeNode(const int tid, Node<K,V> * node, bool retire) {
        if (retire) {
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
            if (node->externalChangeCounter) {
//                GSTATS_ADD(tid, num_multi_counter_node_retired, 1);
                recordmgr->retire(tid, node->externalChangeCounter);
            }
#endif
            recordmgr->retire(tid, node);
        } else {
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
            if (node->externalChangeCounter) {
//                GSTATS_ADD(tid, num_multi_counter_node_deallocated, 1);
                recordmgr->deallocate(tid, node->externalChangeCounter);
            }
#endif
            recordmgr->deallocate(tid, node);
        }
    }

    void freeSubtree(const int tid, casword_t ptr, bool retire, bool tryTimingCall = true) {
#ifdef GSTATS_HANDLE_STATS
        TIMELINE_START_C(tid, tryTimingCall);
        DURATION_START_C(tid, tryTimingCall);
#endif

        if (IS_REBUILDOP(ptr)) {
            auto op = CASWORD_TO_REBUILDOP(ptr);
            freeSubtree(tid, NODE_TO_CASWORD(op->rebuildRoot), retire, false);
            if (retire) {
                recordmgr->retire(tid, op);
            } else {
                recordmgr->deallocate(tid, op);
            }
        } else if (IS_NODE(ptr) && ptr != NODE_TO_CASWORD(NULL) && ptr !=  NODE_TO_CASWORD(UNDONE)) {
            auto node = CASWORD_TO_NODE(ptr);
//            if (node->degree > node->capacity) {
//                std::cout << "Something went wrong14\n";
//                exit(0);
//            }
//            assert(node->degree <= node->capacity);
            for (int i=0;i<node->degree;++i) {
                auto child = prov->readPtr(tid, node->ptrAddr(i));
                freeSubtree(tid, child, retire, false);
            }
            freeNode(tid, node, retire);
        }

#ifdef GSTATS_HANDLE_STATS
        DURATION_END_C(tid, duration_traverseAndRetire, tryTimingCall);
        TIMELINE_END_C(tid, "freeSubtree", tryTimingCall);
#endif
    }

private:

    class IdealBuilder {
    private:
        static const int UPPER_LIMIT_DEPTH = 16;
        size_t initNumKeys;
        istree<K,V,Interpolate,RecManager> * ist;
        size_t depth;
        KVPairWithAccesses<K,V> * pairs;
        size_t pairsAdded;
        casword_t tree;

        Node<K,V> * build(const int tid, KVPairWithAccesses<K,V> * pset, int psetSize, const size_t currDepth, casword_t volatile * constructingSubtree, bool parallelizeWithOMP = false) {
            //here i give pset with special 0 ellement whose key is minKey and
            // special element psetSize + 1 whose key is maxKey
//            std::cerr << tid << ": " << "building " << pset[0].kv.k << " " << pset[psetSize + 1].kv.k << "\n";
            if (psetSize == 0) {
                return NULL;
            }
            if (*constructingSubtree != NODE_TO_CASWORD(UNDONE)) {
//#ifdef GSTATS_HANDLE_STATS
//                GSTATS_ADD_IX(tid, num_bail_from_build_at_depth, 1, (currDepth > 9 ? 9 : currDepth));
//#endif
                return (Node<K, V> *)UNDONE; // bail early if tree was already constructed by someone else
            }


            if (psetSize <= MAX_ACCEPTABLE_LEAF_SIZE) {
//                std::cerr << tid << ": " << "ended building using create leaf " << pset[0].kv.k << " " << pset[psetSize + 1].kv.k << " " << psetSize << "\n";
                return ist->createLeaf(tid, pset, psetSize);
            } else {
//                std::cerr << tid << ": " << "choosing represent on level: \n";
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
                Node<K,V> * node = NULL;
                uint64_t m = pset[psetSize].prefSumAc - pset[0].prefSumAc;
                if (currDepth <= 1) {
                    node = ist->createMultiCounterNode(tid, m, (uint64_t)(pow(double(m), ALPHA)) + 2);
                } else {
                    node = ist->createNode(tid, m, (uint64_t)(pow(double(m), ALPHA)) + 2);
                }
#else
                auto node = ist->createNode(tid, m, (uint64_t)(pow(double(m), ALPHA)) + 2);
#endif
                uint64_t threshold = (uint64_t)pow(m, 1.0 - ALPHA) + 1;
                node->minKey = pset[0].kv.k;
                node->maxKey = pset[psetSize + 1].kv.k;
                KVPairWithAccesses<K,V> * childSet = pset;
                int rest = psetSize;
                size_t i = 0;
                for (i = 0; rest >= 0; i++) {
                    if (*constructingSubtree != NODE_TO_CASWORD(UNDONE)) {
#ifdef GSTATS_HANDLE_STATS
                        GSTATS_ADD_IX(tid, num_bail_from_build_at_depth, 1, (currDepth > 9 ? 9 : currDepth));
#endif
                        return (Node<K, V> *) UNDONE; // bail early if tree was already constructed by someone else
                    }

                    int szL = 0, szR = rest + 1;
                    while (szR - szL > 1) {
                        int mid = (szL + szR) / 2;
                        if (childSet[mid].prefSumAc - childSet[0].prefSumAc >= threshold) {
                            szR = mid;
                        } else {
                            szL = mid;
                        }
                    }
                    Node<K, V>* child = NULL;
                    if (szL > 0)
                        child = build(tid, childSet, szL, 1+currDepth, constructingSubtree);

                    *node->ptrAddr(i) = NODE_TO_CASWORD(child);
                    if (i > 0) {
//                        if (!(i - 1 < (node->capacity) - 1)) {
//                            std::cout << "Something went wrong13\n";
//                            exit(0);
//                        }
//                        if (!(child == NULL || child == (Node<K, V> *) UNDONE || child->degree > 1)) {
//                            std::cout << "Something went wrong13ew\n";
//                            std::cout << szL << " " << child << "\n";
//                            exit(0);
//                        }
//                        assert(child == NULL || child == (Node<K, V> *) UNDONE || child->degree > 1);
//                        assert(i - 1 < (node->capacity) - 1);
                        node->key(i-1) = childSet[0].kv.k;
                        node->val(i-1) = VAL_TO_CASWORD(childSet[0].kv.v);
                        node->access(i-1) = ACCESS_TO_CASWORD(childSet[0].ac);
                    }
#ifndef NDEBUG
//                    if (!(i < 2 || node->key(i-1) > node->key(i-2))) {
//                        std::cout << "Something went wrong12\n";
//                        exit(0);
//                    }
//                    assert(i < 2 || node->key(i-1) > node->key(i-2));
#endif
                    childSet += szR;
                    rest -= szR;
                }
                i += rest;
//                for (int j = 0; j <= psetSize; j++) {
//                    std::cerr << tid << ": " << pset[j].kv.k << " ";
//                }
//                std::cerr << "\n";
//                for (int j = 0; j < i; j++) {
//                    std::cerr << tid << ": " << node->key(j) << " ";
//                }
//                std::cerr << "\n";
                node->degree = i + 1;
//                if (node->degree > node->capacity) {
//                    std::cout << "Something went wrong11\n";
//                    exit(0);
//                }
//                assert(node->degree <= node->capacity);
                node->calcId();
//                std::cerr << tid << ": " << "ended building " << pset[0].kv.k << " " << pset[psetSize + 1].kv.k << "\n";
                return node;
            }
        }
    public:
        IdealBuilder(istree<K,V,Interpolate,RecManager> * _ist, const size_t _initNumKeys, const size_t _depth) {
            initNumKeys = _initNumKeys;
            ist = _ist;
            depth = _depth;
            pairs = new KVPairWithAccesses<K,V>[initNumKeys];
            pairsAdded = 0;
            tree = (casword_t) NULL;
        }
        ~IdealBuilder() {
            delete[] pairs;
        }
        void ___experimental_setNumPairs(const size_t numPairs) {
            pairsAdded = numPairs;
        }
        void ___experimental_addKV(const K& key, const V& value, const size_t index) {
            pairs[index] = {key, value};
        }
        void addKV(const int tid, const K& key, const V& value, const uint64_t ac) {
//            std::cerr << tid << ": " << "adding pair: " << key << " " << value << "\n";
            pairs[pairsAdded].kv = {key, value};
            pairs[pairsAdded].ac = ac;
            pairs[pairsAdded].prefSumAc = ac;
            if (pairsAdded)
                pairs[pairsAdded].prefSumAc += pairs[pairsAdded - 1].prefSumAc;
            pairsAdded++;
//#ifndef NDEBUG
//            if (pairsAdded > initNumKeys) {
////                printf("tid=%d key=%lld pairsAdded=%lu initNumKeys=%lu\n", tid, key, pairsAdded, initNumKeys);
////                ist->debugGVPrint(tid);
//                std::cout << "Something went wrong10\n";
//                exit(0);
//            }
//#endif
//            assert(pairsAdded <= initNumKeys);
        }

        casword_t getCASWord(const int tid, casword_t volatile * constructingSubtree, bool parallelizeWithOMP = false) {
            if (*constructingSubtree != NODE_TO_CASWORD(UNDONE)) return NODE_TO_CASWORD(UNDONE);

            if (!tree) {
                if (unlikely(pairsAdded <= 2)) {
                    tree = NODE_TO_CASWORD(NULL);
                } else {
                    tree = NODE_TO_CASWORD(build(tid, pairs, pairsAdded - 2, depth, constructingSubtree, parallelizeWithOMP));
                }
            }
            if (*constructingSubtree != NODE_TO_CASWORD(UNDONE)) {
                ist->freeSubtree(tid, tree, false);
                return NODE_TO_CASWORD(UNDONE);
            }
            return tree;
        }

        K getMinKey() {
            assert(pairsAdded > 0);
            return pairs[0].k;
        }
    };

    void addKVPairs(const int tid, casword_t ptr, IdealBuilder * b);
    void addKVPairsSubset(const int tid, RebuildOperation<K,V> * op, Node<K,V> * node, K mKey, K maKey, size_t depth, IdealBuilder * b, casword_t volatile * constructingSubtree);
    casword_t createIdealConcurrent(const int tid, RebuildOperation<K,V> * op, const uint64_t accessCount);
    void subtreeBuildAndReplace(const int tid, RebuildOperation<K,V>* op, Node<K,V>* parent, int ix, K mKey, K maKey, uint64_t maxAc);
    void getSplittersConcurrent(const int tid, RebuildOperation<K,V> * op,
                                Node<K, V> * node, Node<K, V> * newRoot, const uint64_t bucketSize, uint64_t prefSum);
    void getSplitters(const int tid, RebuildOperation<K,V> * op,
                      casword_t node, Node<K, V> * newRoot, const uint64_t bucketSize, uint64_t prefSum, int volatile * subtreeToRebuild);
    uint64_t getDirty(int tid, casword_t ptr);
    void calcIdConcurrent(const int tid, RebuildOperation<K,V> * op, Node<K,V> * node);

    int init[MAX_THREADS_POW2] = {0,};
    PAD;
    Random64 threadRNGs[MAX_THREADS_POW2];
    PAD;
    struct MyCouner {
        long long value;
        volatile char pad[128];
        MyCouner() {
            value = 0;
        }
        void inc() {
            value++;
        }
    };
public:
    const K INF_KEY;
    const V NO_VALUE;
    const int NUM_PROCESSES;
    uint64_t cops;
    MyCouner rcnts[MAX_THREADS_POW2];
    PAD;

    void initThread(const int tid) {
//        if (myRNG == NULL) myRNG = new Random64(rand());
        if (init[tid]) return; else init[tid] = !init[tid];

        threadRNGs[tid].setSeed(rand());
//        assert(threadRNGs[tid].next());
        prov->initThread(tid);
        recordmgr->initThread(tid);
    }
    void deinitThread(const int tid) {
//        if (myRNG != NULL) { delete myRNG; myRNG = NULL; }
        if (!init[tid]) return; else init[tid] = !init[tid];

        prov->deinitThread(tid);
        recordmgr->deinitThread(tid);
    }

    istree(const int numProcesses
        , const K infinity
        , const V noValue
    )
        : recordmgr(new RecManager(numProcesses, SIGQUIT))
        , prov(new dcssProvider<void* /* unused */>(numProcesses))
        , INF_KEY(infinity)
        , NO_VALUE(noValue)
        , NUM_PROCESSES(numProcesses)
        , cops(cops)
    {
        srand(time(0)); // for seeding per-thread RNGs in initThread
        cmp = Interpolate();
        cops = 1;
        const int tid = 0;
        initThread(tid);
        KVPairWithAccesses<K, V> p[4];
        p[0].kv = {-INF_KEY - 1, NO_VALUE};
        p[1].kv = {-INF_KEY, NO_VALUE};
        p[2].kv = {INF_KEY, NO_VALUE};
        p[3].kv = {INF_KEY, NO_VALUE};
        p[0].ac = p[1].ac = p[2].ac = p[3].ac = 0;
        p[0].prefSumAc = p[1].prefSumAc = p[2].prefSumAc = p[3].prefSumAc = 0;
        Node<K,V> * _root = createLeaf(tid, p, 2);
        _root->initAc = (1000000000LL) * (1000000000LL);
        root = _root;
    }

    ~istree() {
        freeSubtree(0, NODE_TO_CASWORD(root), false);
////            COUTATOMIC("main thread: deleted tree containing "<<nodes<<" nodes"<<std::endl);
        recordmgr->printStatus();
        delete prov;
        delete recordmgr;
    }

    Node<K,V> * debug_getEntryPoint() { return root; }

    V find(const int tid, const K& key) {
        rcnts[tid].inc();
        if (rcnts[tid].value % cops == 0)
            return doUpdate(tid, key, NO_VALUE, Find);
        return doFind(tid, key);
    }

    void setCops(uint64_t _cops) {
        cops = _cops;
    }

    bool contains(const int tid, const K& key) {
        return find(tid, key);
    }
    V insert(const int tid, const K& key, const V& val) {
        return doUpdate(tid, key, val, InsertReplace);
    }
    V insertIfAbsent(const int tid, const K& key, const V& val) {
        return doUpdate(tid, key, val, InsertIfAbsent);
    }
    V erase(const int tid, const K& key) {
        return doUpdate(tid, key, NO_VALUE, Erase);
    }
    RecManager * const debugGetRecMgr() {
        return recordmgr;
    }
};

template <typename K, typename V, class Interpolate, class RecManager>
void istree<K,V,Interpolate,RecManager>::helpFreeSubtree(const int tid, Node<K,V> * node) {
    // if node is the root of a *large* subtree (256+ children),
    // then have threads *collaborate* by reserving individual subtrees to free.
    // idea: reserve a subtree before freeing it by CASing it to NULL
    //       we are done when all pointers are NULL.

    // conceptually you reserve the right to reclaim everything under a node
    // (including the node) when you set its DIRTY_DIRTY_MARKED_FOR_FREE_MASK bit
    //
    // note: the dirty field doesn't exist for kvpair, value, empty value and rebuildop objects...
    // so to reclaim those if they are children of the root node passed to this function,
    // we claim the entire root node at the end, and go through those with one thread.

#ifdef GSTATS_HANDLE_STATS
    TIMELINE_START(tid);
    DURATION_START(tid);
#endif
//    if (node->degree > node->capacity) {
//        std::cout << "Something went wrong8 deg > cap\n";
//        exit(0);
//    }
//    assert(node->degree <= node->capacity);
    for (int i=0;i<node->degree;++i) {
        auto ptr = prov->readPtr(tid, node->ptrAddr(i));
        if (IS_NODE(ptr)) {
            auto child = CASWORD_TO_NODE(ptr);
            if (child == NULL) continue;

            // claim subtree rooted at child
            while (true) {
                auto old = child->dirty;
                if (IS_DIRTY_MARKED_FOR_FREE(old)) break;
                if (CASB(&child->dirty, old, old | DIRTY_MARKED_FOR_FREE_MASK)) {
                    freeSubtree(tid, ptr, true, false);
                }
            }
        }
    }

    // then try to claim the node itself to handle special object types (kvpair, value, empty value, rebuildop).
    // claim node and its pointers that go to kvpair, value, empty value and rebuildop objects, specifically
    // (since those objects, and their descendents in the case of a rebuildop object,
    // are what remain unfreed [since all descendents of direct child *node*s have all been freed])
    while (true) {
        auto old = node->dirty;
        if (IS_DIRTY_MARKED_FOR_FREE(old)) break;
        if (CASB(&node->dirty, old, old | DIRTY_MARKED_FOR_FREE_MASK)) {
            // clean up pointers to non-*node* objects (and descendents of such objects)
            for (int i=0;i<node->degree;++i) {
                auto ptr = prov->readPtr(tid, node->ptrAddr(i));
                if (!IS_NODE(ptr)) {
                    freeSubtree(tid, ptr, true, false);
                }
            }
            freeNode(tid, node, true); // retire the ACTUAL node
        }
    }

#ifdef GSTATS_HANDLE_STATS
    DURATION_END(tid, duration_traverseAndRetire);
    TIMELINE_END(tid, "freeSubtree");
#endif
}

template <typename K, typename V, class Interpolate, class RecManager>
uint64_t istree<K,V,Interpolate,RecManager>::markAndCount(const int tid, const casword_t ptr, bool tryTiming /* = true by default */) {
#ifdef MEASURE_DURATION_STATS
    TimeThisScope obj (tid, duration_markAndCount, tryTiming);
#endif
//    std::cerr << tid << ": " << "We are doing mark and count!\n";
    if (unlikely(IS_REBUILDOP(ptr))) {
        // if we are here seeing this rebuildop,
        // then we ALREADY marked the node that points to the rebuildop,
        // which means that rebuild op cannot possibly change that node
        // to effect the rebuilding.
        return markAndCount(tid, NODE_TO_CASWORD(CASWORD_TO_REBUILDOP(ptr)->rebuildRoot), false);
    }

//    assert(IS_NODE(ptr));
    auto node = CASWORD_TO_NODE(ptr);

    // optimize by taking the sum from node->dirty if we run into a finished subtree
    if (node == NULL) {
        return 0;
    }
//    if (node->degree > node->capacity)
//    {
//        std::cout << "Something went wrong7 deg > cap\n";
//        exit(0);
//    }
//    assert(node->degree <= node->capacity);
    auto result = node->dirty;
    if (IS_DIRTY_FINISHED(result)) return DIRTY_FINISHED_TO_SUM(result); // markAndCount has already FINISHED in this subtree, and sum is the count

    if (!IS_DIRTY_STARTED(result)) __sync_val_compare_and_swap(&node->dirty, 0, DIRTY_STARTED_MASK);

    // high level idea: if not at a leaf, try to divide work between any helpers at this node
    //      by using fetch&add to "soft-reserve" a subtree to work on.
    //      (each helper will get a different subtree!)
    // note that all helpers must still try to help ALL subtrees after, though,
    //      since a helper might crash after soft-reserving a subtree.
    //      the DIRTY_FINISHED indicator makes these final helping attempts more efficient.
    //
    // this entire idea of dividing work between helpers first can be disabled
    //      by defining IST_DISABLE_COLLABORATIVE_MARK_AND_COUNT
    //
    // can the clean fetch&add work division be adapted better for concurrent ideal tree construction?
    //
    // note: could i save a second traversal to build KVPair arrays by having
    //      each thread call addKVPair for each key it sees in THIS traversal?
    //      (maybe avoiding sort order issues by saving per-thread lists and merging)

#if !defined IST_DISABLE_COLLABORATIVE_MARK_AND_COUNT
    // optimize for contention by first claiming a subtree to recurse on
    // THEN after there are no more subtrees to claim, help (any that are still DIRTY_STARTED)
    if (node->degree > MAX_ACCEPTABLE_LEAF_SIZE) { // prevent this optimization from being applied at the leaves, where the number of fetch&adds will be needlessly high
        while (1) {
            auto ix = __sync_fetch_and_add(&node->nextMarkAndCount, 1);
            if (ix >= node->degree) break;
            markAndCount(tid, prov->readPtr(tid, node->ptrAddr(ix)), false);

            auto result = node->dirty;
            if (IS_DIRTY_FINISHED(result)) return DIRTY_FINISHED_TO_SUM(result); // markAndCount has already FINISHED in this subtree, and sum is the count
        }
    }
#endif

    // recurse over all subtrees
    uint64_t accessCount = 0;
    // TODO: try random permutation here
    for (int i=0;i<node->degree;++i) {
        if (i + 1 < node->degree && CASWORD_TO_VAL(prov->readPtr(tid, node->valAddr(i))) != NO_VALUE) {
            accessCount += CASWORD_TO_ACCESS(prov->readPtr(tid, node->accessAddr(i)));
        }
        accessCount += markAndCount(tid, prov->readPtr(tid, node->ptrAddr(i)), false);

        auto result = node->dirty;
        if (IS_DIRTY_FINISHED(result)) return DIRTY_FINISHED_TO_SUM(result); // markAndCount has already FINISHED in this subtree, and sum is the count
    }

//    std::cerr << tid << ": markedAndCount node: " << node->minKey << " " << node->maxKey << ": " << accessCount << "\n";
    __sync_bool_compare_and_swap(&node->dirty, DIRTY_STARTED_MASK, SUM_TO_DIRTY_FINISHED(accessCount));
    return accessCount;
}

template <typename K, typename V, class Interpolate, class RecManager>
void istree<K,V,Interpolate,RecManager>::addKVPairs(const int tid, casword_t ptr, IdealBuilder * b) {
    if (unlikely(IS_REBUILDOP(ptr))) {
        auto op = CASWORD_TO_REBUILDOP(ptr);
        addKVPairs(tid, NODE_TO_CASWORD(op->rebuildRoot), b);
    } else {
//        assert(IS_NODE(ptr));
        auto node = CASWORD_TO_NODE(ptr);
//        assert(IS_DIRTY_FINISHED(node->dirty) && IS_DIRTY_STARTED(node->dirty));
        for (int i=0;i<node->degree;++i) {
            if (i > 0) {
                auto v = CASWORD_TO_VAL(prov->readPtr(tid, node->valAddr(i - 1)));
                if (v == NO_VALUE)
                    continue;
                b->addKV(node->key(i - 1), v, CASWORD_TO_ACCESS(prov->readPtr(tid, node->accessAddr(i - 1))));
            }
            auto childptr = prov->readPtr(tid, node->ptrAddr(i));
            addKVPairs(tid, childptr, b);
        }
    }
}

template <typename K, typename V, class Interpolate, class RecManager>
void istree<K,V,Interpolate,RecManager>::addKVPairsSubset(const int tid, RebuildOperation<K,V> * op, Node<K,V> * node, K mKey, K maKey, size_t depth, IdealBuilder * b, casword_t volatile * constructingSubtree) {
    if (node == NULL || node->minKey >= maKey || node->maxKey <= mKey)
        return;

    int pos = 0;
    if ((node->minKey) < mKey)
        pos = interpolationSearch(tid, mKey, node);

    if (pos > 0) pos--;
//    assert(node->degree <= node->capacity);
    for (int i=pos;i<node->degree;++i) {

        if ((op->success) || prov->readPtr(tid, constructingSubtree) != NODE_TO_CASWORD(UNDONE)) {
#ifdef GSTATS_HANDLE_STATS
            GSTATS_ADD_IX(tid, num_bail_from_addkv_at_depth, 1, (depth > 9 ? 9 : depth));
#endif
            return; // stop early if someone else built the subtree already
        }

        auto childptr = prov->readPtr(tid, node->ptrAddr(i));
        if (unlikely(IS_REBUILDOP(childptr))) {
            auto child = CASWORD_TO_REBUILDOP(childptr)->rebuildRoot;
//            if (!IS_DIRTY_FINISHED(child->dirty)) {
//                std::cout << "Something went wrong5 is dirty not finished\n";
//                exit(0);
//            }
//            assert(IS_DIRTY_FINISHED(child->dirty));
            addKVPairsSubset(tid, op, child, mKey, maKey, 1+depth, b, constructingSubtree);
        } else if (!IS_EMPTY_CASWORD(childptr)) {
            auto child = CASWORD_TO_NODE(childptr);
            addKVPairsSubset(tid, op, child, mKey, maKey, 1+depth, b, constructingSubtree);
        }
        if (i + 1 < (node->degree)) {

            K k = (node->key(i));
            V v = CASWORD_TO_VAL(prov->readPtr(tid, node->valAddr(i)));
            uint64_t ac = CASWORD_TO_ACCESS(prov->readPtr(tid, node->accessAddr(i)));
            if (k >= maKey)
                break;
            if (v == NO_VALUE)
                continue;
            if (k > mKey)
                b->addKV(tid, k, v, ac);
        }
    }
}

template<typename K, typename V, class Interpolate, class RecManager>
void istree<K, V, Interpolate, RecManager>::subtreeBuildAndReplace(const int tid, RebuildOperation<K,V>* op, Node<K,V>* parent, int ix, K mKey, K maKey, uint64_t maxAc) {
#ifdef GSTATS_HANDLE_STATS
    DURATION_START(tid);
#endif
//    std::cerr << tid << ": " << "Building ideal subtree between: " << mKey << " " << maKey << "\n";
    // build new subtree
    if (mKey == maKey) {
        __sync_bool_compare_and_swap(parent->ptrAddr(ix), NODE_TO_CASWORD(UNDONE), NODE_TO_CASWORD(NULL));
        return;
    }
    IdealBuilder b (this, maxAc, 1+op->depth);

    b.addKV(tid, mKey, NO_VALUE, 0);
    addKVPairsSubset(tid, op, op->rebuildRoot, mKey, maKey, op->depth, &b, parent->ptrAddr(ix)); // construct the subtree
    b.addKV(tid, maKey, NO_VALUE, 0);
    if ((prov->readPtr(tid, parent->ptrAddr(ix))) != NODE_TO_CASWORD(UNDONE)) {
#ifdef GSTATS_HANDLE_STATS
        GSTATS_ADD_IX(tid, num_bail_from_addkv_at_depth, 1, op->depth);
        DURATION_END(tid, duration_wastedWorkBuilding);
#endif
        return;
    }
    auto ptr = b.getCASWord(tid, parent->ptrAddr(ix));
    if (NODE_TO_CASWORD(UNDONE) == ptr) {
#ifdef GSTATS_HANDLE_STATS
        DURATION_END(tid, duration_wastedWorkBuilding);
#endif
        return; // if we didn't build a tree, because someone else already replaced this subtree, then we just stop here (just avoids an unnecessary cas below in this case; apart from this cas, which will fail, the behaviour is no different whether we return here or execute the following...)
    }

//    auto result = prov->dcssPtr(tid, (casword_t *) (op->parent->ptrAddr(op->index)), REBUILDOP_TO_CASWORD(op), (casword_t*)parent->ptrAddr(ix), NODE_TO_CASWORD(NULL), ptr);

    if (__sync_bool_compare_and_swap(parent->ptrAddr(ix), NODE_TO_CASWORD(UNDONE), ptr)) { // try to CAS the subtree in to the new root we are building (consensus to decide who built it)
    } else {
        freeSubtree(tid, ptr, false);
#ifdef GSTATS_HANDLE_STATS
        DURATION_END(tid, duration_wastedWorkBuilding);
#endif
    }
}

template <typename K, typename V, class Interpolate, class RecManager>
uint64_t istree<K,V,Interpolate,RecManager>::getDirty(int tid, casword_t ptr) {
    if (unlikely(IS_REBUILDOP(ptr))) {

        return DIRTY_FINISHED_TO_SUM((CASWORD_TO_REBUILDOP(ptr)->rebuildRoot)->dirty);
    }
    if (CASWORD_TO_NODE(ptr) == NULL)
        return 0;
    return  DIRTY_FINISHED_TO_SUM((CASWORD_TO_NODE(ptr))->dirty);
}

template <typename K, typename V, class Interpolate, class RecManager>
void istree<K,V,Interpolate,RecManager>::getSplitters(const int tid, RebuildOperation<K,V> * op, casword_t node,
    Node<K, V> * newRoot, const uint64_t bucketSize, uint64_t prefSum, int volatile * subtreeToRebuild) {
    if (CASWORD_TO_NODE(node) == NULL) {
        return;
    }
    if (unlikely(IS_REBUILDOP(node))) {

        getSplitters(tid, op, NODE_TO_CASWORD(CASWORD_TO_REBUILDOP(node)->rebuildRoot), newRoot, bucketSize, prefSum, subtreeToRebuild);
        return;
    }
    auto noded = CASWORD_TO_NODE(node);
    for (int i=0;i<noded->degree;++i) {
        if ((*subtreeToRebuild) != 0)
            return;
//        std::cerr << noded->minKey << ":" << prefSum << ":kha:" << tid << ":" << noded->maxKey << "\n";
        auto nxtPtr = prov->readPtr(tid, noded->ptrAddr(i));
        if (CASWORD_TO_NODE(nxtPtr) != NULL)
        {
            getSplitters(tid, op, nxtPtr, newRoot,
                             bucketSize, prefSum, subtreeToRebuild);
            prefSum += getDirty(tid, nxtPtr);
        }

        if (i + 1 < noded->degree) {
            auto vl = CASWORD_TO_VAL(prov->readPtr(tid, noded->valAddr(i)));
            if (vl != NO_VALUE)
            {

                uint64_t kk = (prefSum / bucketSize) * bucketSize + bucketSize;
                uint64_t pos = prefSum / bucketSize;
                uint64_t ccc = CASWORD_TO_ACCESS(prov->readPtr(tid, noded->accessAddr(i)));;
//                std::cout << prefSum + ccc << " " << (noded->key(i)) << " " << bucketSize << " khe\n";
                while (kk <= prefSum + ccc)
                {
//                    std::cout << pos << " " << prefSum + ccc << " " << (noded->key(i)) << " " << bucketSize << " khe\n";
                    __sync_bool_compare_and_swap(newRoot->keyAddr(pos), 0, noded->key(i));
                    if (kk + bucketSize > prefSum + ccc)
                    {
                        __sync_bool_compare_and_swap(newRoot->valAddr(pos), VAL_TO_CASWORD(UNSET_VALUE),
                                                     VAL_TO_CASWORD(vl));
                        __sync_bool_compare_and_swap(newRoot->accessAddr(pos), 0,
                                                     ACCESS_TO_CASWORD(ccc));
                    }
                    else
                    {
                        __sync_bool_compare_and_swap(newRoot->valAddr(pos), VAL_TO_CASWORD(UNSET_VALUE),
                                                     VAL_TO_CASWORD(NO_VALUE));
                    }
                    pos++;
                    kk += bucketSize;
                }
                prefSum += ccc;
            }
        }
    }
    return;
}


template <typename K, typename V, class Interpolate, class RecManager>
void istree<K,V,Interpolate,RecManager>::getSplittersConcurrent(const int tid, RebuildOperation<K,V> * op,
    Node<K, V> * node, Node<K, V> * newRoot, const uint64_t bucketSize, uint64_t prefSum) {
//    printGV(tid);
    if (CASWORD_TO_NODE(node) == NULL) {
        return;
    }
//    if (NODE_TO_CASWORD(node) == UNDONE) {
//        assert(0);
//    }
    uint64_t accessCount = prefSum;
    int lstx = 0;
    while (1) {
        auto ix = node->nextGetSplitter;
        if (ix >= node->degree) break;
        while (lstx < ix) {
            prefSum += getDirty(tid, prov->readPtr(tid, node->ptrAddr(lstx)));
            if (lstx + 1 < (node->degree) && CASWORD_TO_VAL(prov->readPtr(tid, node->valAddr(lstx))) != NO_VALUE)
                prefSum += CASWORD_TO_ACCESS(prov->readPtr(tid, node->accessAddr(lstx)));
            lstx++;
        }
        auto nxtPtr = prov->readPtr(tid, node->ptrAddr(ix));
        if (__sync_bool_compare_and_swap(&node->nextGetSplitter, ix, ix + 1)) {
            getSplitters(tid, op, nxtPtr, newRoot, bucketSize, prefSum, node->splittersAddr(ix));
            __sync_bool_compare_and_swap(node->splittersAddr(ix), 0, 1);
        }
        prefSum += getDirty(tid, nxtPtr);
        if (ix + 1 < node->degree && CASWORD_TO_VAL(prov->readPtr(tid, node->valAddr(ix))) != NO_VALUE)
            prefSum += CASWORD_TO_ACCESS(prov->readPtr(tid, node->accessAddr(ix)));
        lstx = ix + 1;
        if (node->finishedSplitting == 1) {
            return;
        }
    }
    prefSum = accessCount;

    for (int i=0;i<node->degree;++i) {
//        std::cerr << prefSum << ":" << tid << "\n";
        auto nxtPtr = prov->readPtr(tid, node->ptrAddr(i));
        if (CASWORD_TO_NODE(nxtPtr) != NULL)
        {
            getSplitters(tid, op, nxtPtr, newRoot, bucketSize, prefSum, node->splittersAddr(i));
            __sync_bool_compare_and_swap(node->splittersAddr(i), 0, 1);
            prefSum += getDirty(tid, nxtPtr);
        }
//        std::cout << prefSum << " keh " << bucketSize << "\n";
        if (i + 1 < node->degree && CASWORD_TO_VAL(prov->readPtr(tid, node->valAddr(i))) != NO_VALUE) {

            uint64_t kk = (prefSum / bucketSize) * bucketSize + bucketSize;
            uint64_t pos = prefSum / bucketSize;
            uint64_t ccc = CASWORD_TO_ACCESS(prov->readPtr(tid, node->accessAddr(i)));;
//            std::cout << pos << " " << prefSum + ccc << " " << (node->key(i)) << " " << bucketSize << " khe\n";
            while (kk <= prefSum + ccc) {
//                std::cout << pos << " " << prefSum + ccc << " " << (node->key(i)) << " " << bucketSize << " khe\n";
                //TODO: check here?
//                if (newRoot->key(pos) != 0)
//                    break;
                __sync_bool_compare_and_swap(newRoot->keyAddr(pos), 0, node->key(i));
                if (kk + bucketSize > prefSum + ccc)
                {
                    __sync_bool_compare_and_swap(newRoot->valAddr(pos), VAL_TO_CASWORD(UNSET_VALUE),
                                                 prov->readPtr(tid, node->valAddr(i)));
                    __sync_bool_compare_and_swap(newRoot->accessAddr(pos), 0,
                                                 ACCESS_TO_CASWORD(ccc));
                } else {
                    __sync_bool_compare_and_swap(newRoot->valAddr(pos), VAL_TO_CASWORD(UNSET_VALUE),
                                                 VAL_TO_CASWORD(NO_VALUE));
                }
                pos++;
                kk += bucketSize;
            }
            prefSum += ccc;
        }
        if (node->finishedSplitting == 1) return;
    }
//    for (int i = 0; i + 1 < newRoot->realDegree; i++) {
//        std::cout << (newRoot->key(i)) << " " << CASWORD_TO_VAL(newRoot->val(i)) << " khe\n";
//    }
    __sync_bool_compare_and_swap(&node->finishedSplitting, 0, 1);
    return;
}

template <typename K, typename V, class Interpolate, class RecManager>
void istree<K,V,Interpolate,RecManager>::calcIdConcurrent(const int tid, RebuildOperation<K,V> * op, Node<K,V> * node)
{
//    int cl = 0;
//    for (int i = 0; i < capacity; i++) {
//        K k = minKey + (uint64_t)(maxKey - minKey) * i / capacity;
//        while (cl < degree - 1 && key(cl) <= k) {
//            cl++;
//        }
//        id(i) = cl;
//    }

    while (1) {
        auto ix = __sync_fetch_and_add(&(op->nextIdPos), 1);
        if (ix >= node->capacity)
            break;
        int kl = -1;
        int kr = (node->degree) - 1;
        K k = (node->minKey) + (uint64_t)((node->maxKey) - (node->minKey)) * ix / (node->capacity);
        while (kr - kl > 1) {
            int mid = (kl + kr) / 2;
            if ((node->key(mid)) <= k) {
                kl = mid;
            } else {
                kr = mid;
            }
        }
        __sync_bool_compare_and_swap((node->idAddr(ix)), 0, kr);
        if ((op->finishedId) != 0) return;
    }
    for (int i = 0; i < (node->capacity); i++) {
        if ((node->id(i)) != 0) continue;
        if ((op->finishedId) != 0) return;
        int kl = -1;
        int kr = (node->degree) - 1;
        K k = (node->minKey) + (uint64_t)((node->maxKey) - (node->minKey)) * i / (node->capacity);
        while (kr - kl > 1) {
            int mid = (kl + kr) / 2;
            if ((node->key(mid)) <= k) {
                kl = mid;
            } else {
                kr = mid;
            }
        }
        __sync_bool_compare_and_swap((node->idAddr(i)), 0, kr);
    }
    __sync_bool_compare_and_swap(&(op->finishedId), 0, 1);
}

template <typename K, typename V, class Interpolate, class RecManager>
casword_t istree<K,V,Interpolate,RecManager>::createIdealConcurrent(const int tid, RebuildOperation<K,V> * op, const uint64_t accessCount) {
    // Note: the following could be encapsulated in a ConcurrentIdealBuilder class
//std::cerr << tid << ": " << "Creating ideal concurrent!\n";
//std::cerr << tid << ": " << "Accesses: " << accessCount << "\n";
//    fprintf(stderr, "%d: rebRoot: %016lx\n", tid, (uint64_t)op->rebuildRoot);
//    fprintf(stderr, "%d: ind: %d, par: %016lx\n", tid, op->index, (uint64_t)op->parent);
//    fprintf(stderr, "%d: RebOp: %016lx\n", tid, (uint64_t)op);
    if (unlikely(accessCount == 0)) return NODE_TO_CASWORD(NULL);
    uint64_t thr = (uint64_t)(pow(double(accessCount), 1. - ALPHA)) + 1;
    if (thr > accessCount) thr = accessCount;
    if (accessCount < MAX_ACCEPTABLE_LEAF_SIZE) thr = accessCount;
    casword_t word = NODE_TO_CASWORD(NULL);
    casword_t newRoot = op->newRoot;
    if (newRoot == EMPTY_VAL_TO_CASWORD) {
        return NODE_TO_CASWORD(NULL);
    } else
    if (newRoot != NODE_TO_CASWORD(NULL)) {
        word = newRoot;
    } else  {
//        assert(newRoot == NODE_TO_CASWORD(NULL));
        Node<K, V>* node;
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
        if (op->depth <= 1) {
            node = createMultiCounterNode(tid, accessCount, accessCount / thr + 1);
        } else {
#endif
            node = createNode(tid, accessCount, accessCount / thr + 1);
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
        }
#endif

//        std::cerr << tid << ": " << "threshold " << thr << "\n";

        // try to CAS node into the RebuildOp
        node->degree = accessCount / thr + 1;
        node->realDegree = 0;
//        if (op->rebuildRoot->minKey > op->rebuildRoot->maxKey) {
//            std::cerr << "something ww121\n";
//            exit(0);
//        }

        node->minKey = op->rebuildRoot->minKey;
        node->maxKey = op->rebuildRoot->maxKey;
//        std::cerr << tid << ": " << "minKey: " << node->minKey << "\n";
//        std::cerr << tid << ": " << "maxKey: " << node->maxKey << "\n";
//        std::cerr << tid << ": " << "capacity: " << node->capacity << "\n";
        for (int i = 0; i < node->degree; i++) {
            (*node->ptrAddr(i)) = NODE_TO_CASWORD(UNDONE);
        }

//        for (int i = 0; i + 1 < realDegree; i++) {
//            std::cerr << tid << ":" << " ";
//            std::cerr <<node->key(i) << " ";
//        }
//        std::cerr <<tid << ": " << " keys\n";

        word = NODE_TO_CASWORD(node);
//        std::cerr << tid << ": " << word << " word\n";
        if (op->newRoot == EMPTY_VAL_TO_CASWORD) {
            return NODE_TO_CASWORD(NULL);
        } else if (__sync_bool_compare_and_swap(&op->newRoot, NODE_TO_CASWORD(NULL), word)) {

        } else {

            // we failed the newRoot CAS, so we lost the consensus race.
            // someone else CAS'd their newRoot in, so ours is NOT the new root.
            // reclaim ours, and help theirs instead.
            freeSubtree(tid, word, false);

            // try to help theirs
            word = op->newRoot;
            if (word == EMPTY_VAL_TO_CASWORD) {
                return NODE_TO_CASWORD(NULL);
            }
        }
    }
    auto node = CASWORD_TO_NODE(word);

//    std::cout << accessCount << " " << thr << "\n";
//    if (accessCount == 7) exit(0);
//    assert(node != NULL);
    size_t realDegree = (node->degree);
    // opportunistically try to build different subtrees from any other concurrent threads
    // by synchronizing via node->degree. concurrent threads increment node->degree using cas
    // to "reserve" a subtree to work on (not truly exclusively---still a lock-free mechanism).
    getSplittersConcurrent(tid, op, op->rebuildRoot, node, thr, 0);
    calcIdConcurrent(tid, op, node);
//    node->calcIdConcurrent();
//    for (int i = 0; i + 1 < realDegree; i++) {
//        int kk = node->key(i);
//        if (kk == 0) {
//            std::cout << "something ww12242\n";
//            std::cerr << accessCount << " " << thr << "koh\n";
//            std::cerr << (op->rebuildRoot->minKey) << " kk " << (op->rebuildRoot->maxKey) << "\n";
//            std::cerr << kk << "\n";
//
//            for (int i = 0; i + 1 < realDegree; i++) {
//                std::cerr << tid << ":" << (node->key(i)) << " " << CASWORD_TO_VAL(node->val(i)) << " " << CASWORD_TO_ACCESS(node->access(i)) << " : khe,";
//            }
//            std::cerr << "\n";
//            printGV(tid);
//            exit(0);
//        }
//    }
//    for (int i = 0; i + 1 < node->realDegree; i++) {
//        std::cout << (node->key(i)) << " " << CASWORD_TO_VAL(node->val(i)) << " khe\n";
//    }
//    node->degree = 0;

    while (1) {
        auto ix = node->realDegree;
        if (ix >= realDegree) break;                                        // skip to the helping phase if all subtrees are already being constructed
        if (__sync_bool_compare_and_swap(&node->realDegree, ix, 1+ix)) {            // use cas to soft-reserve a subtree to construct
            auto mKey = node->minKey;
            if (ix > 0) mKey = node->key(ix - 1);
            auto maKey = node->maxKey;
            if (ix + 1 < realDegree) maKey = node->key(ix);

//            std::cerr << tid << ": " << "Building subtree between "<< mKey << " " <<  maKey << "\n";
            subtreeBuildAndReplace(tid, op, node, ix, mKey, maKey, thr + 1);
//            std::cerr << tid << ": " << "Builded subtree between "<< mKey << " " <<  maKey << "\n";
        }
    }

    // try to help complete subtree building if necessary
    // (partially for lock-freedom, and partially for performance)

    // help linearly starting at a random position (to probabilistically scatter helpers)

    for (int __i=0;__i<realDegree;++__i) {
        auto i = __i;//+ix) % realDegree;
        if (op->success) break;
        if (prov->readPtr(tid, node->ptrAddr(i)) == NODE_TO_CASWORD(UNDONE)) {
        //here something other should be because for our data structure it can be null
            auto mKey = node->minKey;
            if (i > 0) mKey = node->key(i - 1);
            auto maKey = node->maxKey;
            if (i + 1 < realDegree) maKey = node->key(i);
//            std::cerr << tid << ": " << "Building subtree between "<< mKey << " " <<  maKey << "\n";
            subtreeBuildAndReplace(tid, op, node, i,  mKey, maKey, thr + 1);
//            std::cerr << tid << ": " << "Builded subtree between "<< mKey << " " <<  maKey << "\n";
#ifdef GSTATS_HANDLE_STATS
            GSTATS_ADD(tid, num_help_subtree, 1);
#endif
        }
    }
    return word;
}

template <typename K, typename V, class Interpolate, class RecManager>
void istree<K,V,Interpolate,RecManager>::helpRebuild(const int tid, RebuildOperation<K,V> * op) {
#ifdef GSTATS_HANDLE_STATS
    TIMELINE_START_C(tid, (op->depth < 1));
#endif

#ifdef MEASURE_REBUILDING_TIME
    //    GSTATS_TIMER_RESET(tid, timer_rebuild);
    GSTATS_ADD(tid, num_help_rebuild, 1);
#endif
//    assert(!recordmgr->isQuiescent(tid));
//    std::cerr << tid << ": helping rebuild: " << op->rebuildRoot->minKey << " " << op->rebuildRoot->maxKey << "\n";
    auto accessCount = markAndCount(tid, NODE_TO_CASWORD(op->rebuildRoot));

//    std::cerr << tid << ": marked and count: " << op->rebuildRoot->minKey << " " << op->rebuildRoot->maxKey << "\n";
    auto oldWord = REBUILDOP_TO_CASWORD(op);


    casword_t newWord = createIdealConcurrent(tid, op, accessCount);
    if (newWord == NODE_TO_CASWORD(NULL) && accessCount > 0) {
        return; // someone else already *finished* helping
    }
    auto result = prov->dcssPtr(tid, (casword_t *) &op->parent->dirty, 0, (casword_t *) op->parent->ptrAddr(op->index), oldWord, newWord).status;
    if (result == DCSS_SUCCESS) {
        SOFTWARE_BARRIER;
//        std::cerr << tid << ": successfully cases\n";
//        assert(op->success == false);
        op->success = 1;
//        __sync_bool_compare_and_swap(&op->newRoot, EMPTY_VAL_TO_CASWORD);
//        op->newRoot = EMPTY_VAL_TO_CASWORD;-
        SOFTWARE_BARRIER;
#ifdef GSTATS_HANDLE_STATS
        GSTATS_ADD_IX(tid, num_complete_rebuild_at_depth, 1, op->depth);
#endif
//        freeSubtree(tid, NODE_TO_CASWORD(op->rebuildRoot), true);
//        helpFreeSubtree(tid, op->rebuildRoot);
        recordmgr->retire(tid, op); // note: it's okay to retire this before reading op-> fields below!!! (this is because retire means don't deallocate until AFTER our memory guard section)
    } else {
        // if we fail to CAS, then either:
        // 1. someone else CAS'd exactly newWord into op->parent->ptrAddr(op->index), or
        // 2. this rebuildop is part of a subtree that is marked and rebuilt by another rebuildop,
        //    and this DCSS failed because op->parent->dirty == 1.
        //    in this case, we should try to reclaim the subtree at newWord.
        //
        if (result == DCSS_FAILED_ADDR1) {
            // [[failed because dirty (subsumed by another rebuild operation)]]
            // note: a rebuild operation should almost never be subsumed by one started higher up,
            // because it's unlikely that while we are trying to
            // rebuild one subtree another rebuild just so happens to start above
            // (since one will only start if it was ineligible to start when we began our own reconstruction,
            //  then enough operations are performed to make a higher tree eligible for rebuild,
            //  then we finish our own rebuilding and try to DCSS our new subtree in)
            // to test this: let's measure whether this happens...
            // apparently it does happen... in a 100% update workload for 15sec with 192 threads, we have: sum rebuild_is_subsumed_at_depth by_index=0 210 1887 277 5
            //      these numbers represent how many subsumptions happened at each depth (none at depth 0 (impossible), 210 at depth 1, and so on).
            //      regardless, this is not a performance issue for now. (at most 3 of these calls took 10ms+; the rest were below that threshold.)
            //      *if* it becomes an issue then helpFreeSubtree or something like it should fix the problem.

// #ifdef GSTATS_HANDLE_STATS
//             GSTATS_ADD(tid, rebuild_is_subsumed, 1); // if this DOES happen, it will be very expensive (well, *if* it's at the top of the tree...) because ONE thread will do it, and this will delay epoch advancement greatly
//             GSTATS_ADD_IX(tid, rebuild_is_subsumed_at_depth, 1, op->depth);
// #endif

            // try to claim the NEW subtree located at op->newWord for reclamation
            if (op->newRoot != NODE_TO_CASWORD(NULL)
                && __sync_bool_compare_and_swap(&op->newRoot, newWord, EMPTY_VAL_TO_CASWORD)) {
                freeSubtree(tid, newWord, true);
                // note that other threads might be trying to help our rebuildop,
                // and so might be accessing the subtree at newWord.
                // so, we use retire rather than deallocate.
            }
            // otherwise, someone else reclaimed the NEW subtree
//            assert(op->newRoot == (casword_t)NULL);
        } else {
//            assert(result == DCSS_FAILED_ADDR2);
        }
    }
#ifdef GSTATS_HANDLE_STATS
    DURATION_END(tid, duration_buildAndReplace);
#endif

//#ifdef MEASURE_REBUILDING_TIME
//    auto cappedDepth = std::min((size_t) 9, op->depth);
//    GSTATS_ADD_IX(tid, elapsed_rebuild_depth, GSTATS_TIMER_ELAPSED(tid, timer_rebuild), cappedDepth);
//#endif

#ifdef GSTATS_HANDLE_STATS
    TIMELINE_END_C(tid, "helpRebuild", (op->depth < 1));
#endif

    // collaboratively free the old subtree, if appropriate (if it was actually replaced)
    if (op->success) {
        if (op->rebuildRoot->degree < 256) {
            if (result == DCSS_SUCCESS) {
                // this thread was the one whose DCSS operation performed the actual swap
                freeSubtree(tid, NODE_TO_CASWORD(op->rebuildRoot), true);
            }
        } else {
#ifdef IST_DISABLE_COLLABORATIVE_FREE_SUBTREE
            if (result == DCSS_SUCCESS) freeSubtree(tid, NODE_TO_CASWORD(op->rebuildRoot), true);
#else
            helpFreeSubtree(tid, op->rebuildRoot);
#endif
        }
    }

#ifdef IST_DISABLE_REBUILD_HELPING
    op->debug_sync_in_experimental_no_collaboration_version = 2;
#endif
}

template <typename K, typename V, class Interpolate, class RecManager>
void istree<K,V,Interpolate,RecManager>::rebuild(const int tid, Node<K,V> * rebuildRoot, Node<K,V> * parent, int indexOfRebuildRoot /* in parent */, const size_t depth) {
//    assert(!recordmgr->isQuiescent(tid));
//    std::cerr <<tid << ": " << "Starting to rebuild!\n";
//    fprintf(stderr, "%d: rebRoot: %016lx\n", tid, (uint64_t)rebuildRoot);
//    fprintf(stderr, "%d: ind: %d, par: %016lx\n", tid, indexOfRebuildRoot, (uint64_t)parent);
//    assert(rebuildRoot != NULL);
    auto op = new RebuildOperation<K,V>(rebuildRoot, parent, indexOfRebuildRoot, depth);
    auto ptr = REBUILDOP_TO_CASWORD(op);
    auto old = NODE_TO_CASWORD(op->rebuildRoot);
//    assert(op->parent == parent);
    auto result = prov->dcssPtr(tid, (casword_t *) &op->parent->dirty, 0, (casword_t *) op->parent->ptrAddr(op->index), old, ptr).status;
    if (result == DCSS_SUCCESS) {
//        fprintf(stderr, "%d: old: %016lx, new: %016lx", tid, (uint64_t)CASWORD_TO_NODE(old), (uint64_t)op);
        helpRebuild(tid, op);
    } else {
        // in this case, we have exclusive access to free op.
        // this is because we are the only ones who will try to perform a DCSS to insert op into the data structure.
//        assert(result == DCSS_FAILED_ADDR1 || result == DCSS_FAILED_ADDR2);
        recordmgr->deallocate(tid, op);
    }
}

template <typename K, typename V, class Interpolate, class RecManager>
int istree<K,V,Interpolate,RecManager>::interpolationSearch(const int tid, volatile K  key, Node<K,V> * const node) {
//    __builtin_prefetch(&node->minKey, 1);
//    __builtin_prefetch(&node->maxKey, 1);

    // these next 3 prefetches are shockingly effective... 20% performance boost in some large scale search-only workloads... (reducing L3 cache misses by 2-3 per search...)
//    __builtin_prefetch(&(node->key(0)), 1);
//    __builtin_prefetch(&(node->keyAddr(0))+(8), 1);
//    __builtin_prefetch((node->keyAddr(0))+(16), 1);

//    if (node == NULL) {
//        std::cout << "Something wwWew5\n";
//        exit(0);
//    }
    auto deg = node->degree;
//    if (node->degree > node->capacity) {
//        std::cout << "Something wwW5\n";
//        exit(0);
//    }
//    assert(node->degree <= node->capacity);
    //assert(node->degree >= 1);
    if (unlikely(deg == 1)) {
//        GSTATS_APPEND_D(tid, visited_in_isearch, 1);
        return 0;
    }

    const int lenId = (node->capacity);
    const K& minKey = node->minKey;
    const K& maxKey = node->maxKey;

//    if (unlikely(key < minKey)) {
////        GSTATS_APPEND_D(tid, visited_in_isearch, 1);
//        return 0;
//    }
//    if (unlikely(key >= maxKey)) {
////        GSTATS_APPEND_D(tid, visited_in_isearch, 1);
//        return lenId;
//    }
    // assert: minKey <= key < maxKey
//    if ((minKey > key) ||  (key >= maxKey)) {
//        std::cout << "Something wwW5ew\n";
//        std::cerr << tid << ":" << minKey << " " << key << " " << maxKey << "\n";
//        assert(0);
//        exit(0);
//    }
//    assert(minKey <= key && key < maxKey);
    int idIx = ((long long)lenId * (key - minKey) / (maxKey - minKey));
//    std::cerr << tid << ": " << idIx << "\n";
//    __builtin_prefetch((node->idAddr(0)) + idIx, 1);
    int ix = node->id(idIx);
//    std::cerr << tid << ": " << ix << " :ix\n";
    if (ix == (node->degree) - 1 && node->key(ix - 1) > key) {
        ix--;
    }
    if (ix == (node->degree) - 1)
        return ix;
//    __builtin_prefetch((node->keyAddr(0))+(ix-8), 1);                           // prefetch approximate key location
//    __builtin_prefetch((node->keyAddr(0))+(ix), 1);                             // prefetch approximate key location
//    __builtin_prefetch((node->keyAddr(0))+(ix+8), 1);                           // prefetch approximate key location

    const K volatile& ixKey = node->key(ix);
//    std::cerr<<tid << ": " << "key="<<key<<" minKey="<<minKey<<" maxKey="<<maxKey<<" ix="<<ix<<" ixKey="<<ixKey<<std::endl;
//    int cnt = 0;
    if (key < ixKey) {
        // search to the left for node.key[i] <= key, then return i+1
        int i;
        for (i=ix-1;i>=0;--i) {
//            sum_steps++;
            if (unlikely(key >= node->key(i))) {
//                GSTATS_APPEND_D(tid, visited_in_isearch, (ix-1) - i + 1);
                return i+1;
            }
//            cnt++;
//            if ((1LL << cnt) > (node->degree)) break;
        }
//        if ((1LL << cnt) > (node->degree)) {
//            int l = -1, r = (node->degree) - 1;
//            while (r - l > 1) {
//                int mid = (l + r) / 2;
//                if ((node->key(mid)) > key) {
//                    r = mid;
//                } else {
//                    l = mid;
//                }
//            }
//            return r;
//        }
        return 0;
    } else {
        int i;
        for (i=ix+1;i<(node->degree) - 1;++i) { // recall: degree - 1 keys vs degree pointers
//            sum_steps++;
            if (unlikely(key < node->key(i))) {
//                GSTATS_APPEND_D(tid, visited_in_isearch, i - (ix+1) + 1);
                return i;
            }
//            cnt++;
//            if ((1LL << cnt) > (node->degree)) break;
        }
//        if ((1LL << cnt) > (node->degree)) {
//            int l = -1, r = (node->degree) - 1;
//            while (r - l > 1) {
//                int mid = (l + r) / 2;
//                if ((node->key(mid)) > key) {
//                    r = mid;
//                } else {
//                    l = mid;
//                }
//            }
//            return r;
//        }
        return (node->degree) - 1;
//        assert(false); return -1;
    }
}

// note: val is unused if t == Erase
template <typename K, typename V, class Interpolate, class RecManager>
V istree<K,V,Interpolate,RecManager>::doUpdate(const int tid, const K& key, const V& val, UpdateType t) {
//    assert(init[tid]);
//    std::cerr <<"\n\nUpdate op received: " <<  tid << " " << key << " ";
//    if (t == Find)
//        std::cerr << "0";
//    if (t == InsertIfAbsent)
//        std::cerr << "1";
//    if (t == Erase)
//        std::cerr << "2";
//    std::cerr << "\n";
    const int MAX_PATH_LENGTH = 64; // in practice, the depth is probably less than 10 even for many billions of keys. max is technically nthreads + O(log log n), but this requires an astronomically unlikely event.
    Node<K,V> * path[MAX_PATH_LENGTH]; // stack to save the path
    int pathLength;
    Node<K,V> * node;

    retry:
    pathLength = 0;
    auto guard = recordmgr->getGuard(tid);
    node = root;
    K mKey = root->minKey;
    K maKey = root->maxKey;
    while (true) {
//        std::cerr << tid<<"searching into a new node!\n";
//        if (node == NULL || IS_REBUILDOP(NODE_TO_CASWORD(node))) {
//            std::cout << "WTFF 1\n";
//            exit(0);
//        }
//        if (((uint64_t)node) < (0x100000000)) {
//            std::cout << "wtff 2\n";
//            fprintf(stderr, "nodes%016lxnodes\n", (uint64_t)node);
//            exit(0);
//        }
        auto ix = interpolationSearch(tid, key, node); // search INSIDE one node
//        std::cerr << tid << ":" << node->minKey << " " << node->maxKey << "\n" << tid << ": ";
//        for (int i = 0; i + 1 < (node->degree); i++) {
//            std::cerr << node->key(i) << " ";
//        }
//        std::cerr << "keys\n";
//        std::cerr << tid << " " << ix << "\n";

        retryNode:
        bool affectsChangeSum = true;
        auto word = prov->readPtr(tid, (casword_t *)node->ptrAddr(ix));
//        fprintf(stderr, "%p\n", CASWORD_TO_NODE(word));
//        fflush(stderr);
        if (IS_REBUILDOP(word)) {
            helpRebuild(tid, CASWORD_TO_REBUILDOP(word));
            goto retry;
        } else {
//            if (((uint64_t)CASWORD_TO_NODE(word)) < (0x100000000) && ((uint64_t)CASWORD_TO_NODE(word)) > 0) {
//
//                std::cout << "wtff 3\n";
//                fprintf(stderr, "%d: wtf_id: %d, wtf_par: %016lx\n", tid, ix, (uint64_t)node);
//                fprintf(stderr, "%dnodes%016lxnodes\n", tid, (uint64_t)CASWORD_TO_NODE(word));
//                exit(0);
//            }
            Node<K,V> * newNode = NULL;
            auto newWord = (casword_t) NULL;

            auto foundKey = ix > 0 ? node->key(ix - 1) : node->maxKey;
            V foundVal;
            dcssresult_t result;
            uint64_t chng = 1;
            if (foundKey == key) {
                auto word2 = prov->readPtr(tid, node->valAddr(ix - 1));
                foundVal = CASWORD_TO_VAL(word2);
                auto acc = prov->readPtr(tid, node->accessAddr(ix - 1));

                auto valAcc = CASWORD_TO_ACCESS(acc);
                if (t == Find || t == Erase) {
                    if (foundVal == NO_VALUE) return NO_VALUE;
                    if (t == Erase)
                    {
                        chng = valAcc;
                        newWord = VAL_TO_CASWORD(NO_VALUE);
                        result = prov->dcssPtr(tid, (casword_t *) &node->dirty, 0, (casword_t *) node->valAddr(ix - 1), word2, newWord);
                    } else
                    {
                        valAcc++;
                        result = prov->dcssPtr(tid, (casword_t *) &node->dirty, 0,
                                               (casword_t *) node->accessAddr(ix - 1), acc,  ACCESS_TO_CASWORD(valAcc));
                    }
                } else
                {
                    if (t == InsertReplace)
                    {
                        newWord = VAL_TO_CASWORD(val);
                    }
                    else
                    {
                        if (foundVal != NO_VALUE) return foundVal;
                        newWord = VAL_TO_CASWORD(val);
                    }
//                    std::cerr << tid << " we come to replace value!\n";
                    result = prov->dcssPtr(tid, (casword_t *) &node->dirty, 0, (casword_t *) node->valAddr(ix - 1), word2, newWord);
                    switch (result.status)
                    {
                        case DCSS_FAILED_ADDR2: // retry from same node
                            goto retryNode;
//                            goto retry;
                            break;
                        case DCSS_FAILED_ADDR1: // node is dirty; retry from root
                            goto retry;
                            break;
                        case DCSS_SUCCESS:
                            break;
                        default:
                            setbench_error("impossible switch case");
                            break;
                    }
                }

            } else {
//                if (node->degree > node->capacity) {
//                    std::cout << "Something wwW4\n";
//                    exit(0);
//                }
//                assert(node->degree <= node->capacity);
                if (ix > 0) mKey = node->key(ix - 1);
                if (ix < node->degree - 1) maKey = node->key(ix);
                if (!IS_EMPTY_CASWORD(word)) {
                    auto prev_node = node;
                    node = CASWORD_TO_NODE(word);
//                    assert(node->minKey == mKey);
//                    assert(node->maxKey == maKey);
//                    if (!(node->minKey <= key && node->maxKey > key)) {
//                        std::cerr << key << "\n";
//                        std::cerr << tid << ":" << prev_node->minKey << " " << prev_node->maxKey << "\n" << tid << ": ";
//                        for (int i = 0; i + 1 < (prev_node->degree); i++) {
//                            std::cerr << prev_node->key(i) << " ";
//                        }
//                        std::cerr << "keys\n";
//                        std::cerr << tid << " " << (prev_node->degree) << " " << ix << " " << prev_node->key(ix) << "\n";
//                        exit(0);
//                    }
//
//
//                    assert(node->minKey <= key && node->maxKey > key);
                    path[pathLength++] = node;
//                    std::cerr<< tid << ": " << "come here!\n";
//                    for (int i = 0; i + 1 < (node->degree); i++) {
//                        std::cerr << tid << ": " << (node->key(i)) << " ";
//                    }
//                    std::cerr << " keys\n";
//                    std::cerr << tid << ": " << mKey << " " << maKey << "\n";
//                    if (pathLength > MAX_PATH_LENGTH) {
//                        std::cout << "Something wwW3\n";
//                        exit(0);
//                    }
//                    assert(pathLength <= MAX_PATH_LENGTH);
                    continue;
                }
                if (t == InsertReplace || t == InsertIfAbsent) {
//                    std::cerr << tid << ": " << "Inserting new key: " << key << "\n";
                    KVPairWithAccesses<K,V> pairs[3];
                    pairs[0].kv.k = mKey;
                    pairs[2].kv.k = maKey;

                    pairs[1].kv.k = key;
                    pairs[1].kv.v = val;
                    pairs[1].ac = 1;
                    pairs[1].prefSumAc = 1;
                    pairs[0].prefSumAc = 0;
                    pairs[2].prefSumAc = 1;

                    newNode = createLeaf(tid, pairs, 1);
                    newWord = NODE_TO_CASWORD(newNode);
                    foundVal = NO_VALUE;
                } else {
                    return NO_VALUE;
                }
                result = prov->dcssPtr(tid, (casword_t *) &node->dirty, 0, (casword_t *) node->ptrAddr(ix), word, newWord);
                if (result.status == DCSS_SUCCESS) {
//                    fprintf(stderr, "%d: old:%016lx, new:%016lx", tid, (uint64_t)CASWORD_TO_NODE(word), (uint64_t)newNode);
                }
            }
            switch (result.status) {
                case DCSS_FAILED_ADDR2: // retry from same node
                    if (newNode) freeNode(tid, newNode, false);
                    goto retryNode;
//                    goto retry;
                    break;
                case DCSS_FAILED_ADDR1: // node is dirty; retry from root
                    if (newNode) freeNode(tid, newNode, false);
                    goto retry;
                    break;
                case DCSS_SUCCESS:
//                    if (t != Find) {
//                        chng *= cops;
//                    }
                    for (int i=1;i<pathLength;++i) {
                        path[i]->increaseChangeSum(tid, &threadRNGs[tid], chng);
                    }

                    // now, we must determine whether we should rebuild
                    for (int i=1;i<pathLength;++i) {
                        if (path[i]->readChangeSum(tid, &threadRNGs[tid] /*myRNG*/) >= REBUILD_FRACTION * path[i]->initAc) {
//                            debugGVPrint(tid);

//                            std::cerr << tid << ": " << i << " " << path[i]->readChangeSum(tid, &threadRNGs[tid] /*myRNG*/) << " ch sum " << path[i]->initAc << "\n";
                            auto parent = path[i-1];
//                            assert(parent->degree > 1);
//                            assert(path[i]->degree > 1);
//                            if (path[i]->degree == 1) {
//                                std::cout << i << "\n";
//                                std::cout << "WTF12320!!!\n";
//                                exit(0);
//                            }
                            auto index = interpolationSearch(tid, path[i]->key(0), parent);

#ifndef NO_REBUILDING
#   ifdef GSTATS_HANDLE_STATS
                            GSTATS_ADD_IX(tid, num_try_rebuild_at_depth, 1, i);
#   endif
//                            assert(path[i]);
//                            if (key == 3) {
//                                printGV(tid);
//                                std::cout << path[i]->minKey << " " << path[i]->maxKey << "\n";
////                                exit(0);
//
//                            }
//                            printGV(tid);
//                            std::cout << path[i]->minKey << " " << path[i]->maxKey << "\n";
                            rebuild(tid, path[i], parent, index, i - 1);
#endif
//                            if (key == 3465) {
//                                exit(0);
//                            }
//                            if (key == 3) {
//                                printGV(tid);
//                                std::cout << path[i]->minKey << " " << path[i]->maxKey << "\n";
//                                exit(0);
//                            }

                            break;
                        }
                    }
                    break;
                default:
                    setbench_error("impossible switch case");
                    break;
            }
            return foundVal;
        }
    }
}

template <typename K, typename V, class Interpolate, class RecManager>
Node<K,V>* istree<K,V,Interpolate,RecManager>::createNode(const int tid, const uint64_t initAc, const uint64_t expDegree) {
//    if (expDegree <= 0) {
//        std::cout << "Something ww12222\n";
//        exit(0);
//    }
//    assert(expDegree > 0);
//    std::cout << sizeof(Node<K, V>) << " " << sizeof(casword_t) << " " << sizeof(int) << "\n" ;
    size_t sz = sizeof(Node<K,V>) + sizeof(casword_t) * (expDegree - 1)
                + sizeof(casword_t) * (expDegree - 1)
                + sizeof(casword_t) * expDegree + sizeof(K) * (expDegree - 1) + sizeof(int) * (expDegree)
                + sizeof(int) * (expDegree);
    if (sz % 8) sz += 8 - sz % 8;
    Node<K,V> * node = (Node<K,V> *)(new uint64_t[sz / 8]); //(Node<K,V> *) new char[sz];
    for (int i = 0; i < sz; i++) {
        (((char*)(node))[i]) = 0;
    }
    for (int i = 0; i < int(expDegree) - 1; i++) {
        node->val(i) = VAL_TO_CASWORD(UNSET_VALUE);
    }
//    std::cerr<<"node of degree "<<degree<<" allocated size "<<sz<<" @ "<<(size_t) node<<std::endl;
//    assert((((size_t) node) & TOTAL_MASK) == 0);
    node->capacity = expDegree;
    node->degree = 0;
    node->initAc = initAc;
    node->changeSum = 0;
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
    node->externalChangeCounter = NULL;
//    assert(!node->externalChangeCounter);
#endif
    node->dirty = 0;
    node->nextMarkAndCount = 0;
//    assert(node);
    return node;
}

template <typename K, typename V, class Interpolate, class RecManager>
Node<K,V>* istree<K,V,Interpolate,RecManager>::createLeaf(const int tid, KVPairWithAccesses<K,V> * pairs, int numPairs) {
//    std::cerr << tid << ": " << "creating leaf\n";
    auto node = createNode(tid, pairs[numPairs].prefSumAc - pairs[0].prefSumAc, numPairs+1);
//    std::cerr << tid << ": " << "creating leaf2\n";
    node->degree = numPairs+1;
    for (int i=1;i<=numPairs;++i) {
//        assert(pairs[i].kv.k > pairs[i-1].kv.k);
//        std::cerr << tid << ": " << (node->capacity) << " " << i << " " << numPairs << "\n";
        node->key(i-1) = pairs[i].kv.k;
        node->val(i-1) = VAL_TO_CASWORD(pairs[i].kv.v);
        node->access(i-1) = ACCESS_TO_CASWORD(pairs[i].ac);
    }

    node->minKey = pairs[0].kv.k;
    node->maxKey = pairs[numPairs + 1].kv.k;
//    std::cerr << tid << ": " << "we come before id calc\n";
    node->calcId();
//    std::cerr << tid << ": " << "we ended creating a leaf\n";
    return node;
}

template <typename K, typename V, class Interpolate, class RecManager>
Node<K,V>* istree<K,V,Interpolate,RecManager>::createMultiCounterNode(const int tid, const uint64_t initAc, const uint64_t degree) {
    //    GSTATS_ADD(tid, num_multi_counter_node_created, 1);
    auto node = createNode(tid, initAc, degree);
#ifndef IST_DISABLE_MULTICOUNTER_AT_ROOT
    node->externalChangeCounter = new MultiCounter(this->NUM_PROCESSES, 1);
//    std::cerr<<"created MultiCounter at address "<<node->externalChangeCounter<<std::endl;
//    assert(node->externalChangeCounter);
#endif
    return node;
}

template <typename K, typename V, class Interpolate, class RecManager>
V istree<K, V, Interpolate, RecManager>::doFind(const int tid, const K & key)
{
    Node<K,V> * node;
    auto guard = recordmgr->getGuard(tid);
    node = root;
    while (true)
    {
        auto ix = interpolationSearch(tid, key, node); // search INSIDE one node
        auto word = prov->readPtr(tid, (casword_t *) node->ptrAddr(ix));
        if (unlikely(IS_REBUILDOP(word)))
        {
            node = CASWORD_TO_NODE(CASWORD_TO_REBUILDOP(word)->rebuildRoot);
        }
        else
        {
            auto foundKey = ix > 0 ? node->key(ix - 1) : node->maxKey;
            V foundVal;
            if (foundKey == key)
            {
                auto word2 = prov->readPtr(tid, node->valAddr(ix - 1));
                foundVal = CASWORD_TO_VAL(word2);
                return foundVal;
            }
            else
            {
                if (!IS_EMPTY_CASWORD(word))
                {
                    node = CASWORD_TO_NODE(word);
                    continue;
                }
                return NO_VALUE;
            }
        }
    }
}


#endif /* EXT_SA_IST_LF_IMPL_H */