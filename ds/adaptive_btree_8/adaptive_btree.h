#pragma once

#include <bits/stdc++.h>

namespace {
    const int INITIAL_MAX_QUERIES = 400;
}

template<typename V>
struct ValueData {
    V value;
    int accesses;
};

template<typename K, typename V, int MAX_KEYS>
struct Node {
    Node(int64_t accesses) : max_queries(std::max((int64_t)100, accesses + 1)) {
        std::memset(children, 0, sizeof(children));
    };

    int size = 0;
    int64_t queries = 0;
    const int64_t max_queries;

    K keys[MAX_KEYS];
    Node* children[MAX_KEYS + 1];
    ValueData<V> data[MAX_KEYS];

    int find_in_node(K const& key) { // return min pos : keys[pos] >= key
        int pos;
        for (pos = 0; pos < size; pos++)
            if (keys[pos] >= key) return pos;
        return pos;
    }

    void insert(K const& key, V const& value, int pos) {
        assert(size + 1 <= MAX_KEYS);
        for (int i = size - 1; i >= pos; i--) {
            keys[i + 1] = keys[i];
            data[i + 1] = data[i];
        }

        keys[pos] = key;
        data[pos] = ValueData<V> { value, 1 };

        size++;
    }

    void remove(int pos) {
        data[pos].accesses = -data[pos].accesses;
    }

    bool new_query() {
        queries++;
        return queries > max_queries;
    }
};

template <typename K, typename V, int MAX_KEYS>
Node<K, V, MAX_KEYS>* build_ideal_tree(std::vector<std::pair<K, ValueData<V>>> const& keys, int l, int r, std::vector<int64_t> const& pref_sum) {
    int size = r - l;
    if (size == 0 && (l != 0 || r != keys.size())) return nullptr;
    Node<K, V, MAX_KEYS> *node = new Node<K, V, MAX_KEYS>(pref_sum[r] - pref_sum[l]);

    if (size <= MAX_KEYS) {
        node->size = size;
        for (int i = 0; i < size; i++) {
            node->keys[i] = keys[l + i].first;
            node->data[i] = keys[l + i].second;
        }
        return node;
    }

    int64_t total_accesses = pref_sum[r] - pref_sum[l];
    node->size = MAX_KEYS;
    int previous_pos = l - 1;
    int pos_left = l;
    for (int i = 0; i < MAX_KEYS; i++) {
        int64_t expected_accesses = (total_accesses * (int64_t)(i + 1)) / (MAX_KEYS + 1);
        int pos_right = r;
        while (pos_left + 1 < pos_right) {
            int mid = (pos_left + pos_right) / 2;
            if (pref_sum[mid] - pref_sum[l] >= expected_accesses)
                pos_right = mid;
            else
                pos_left = mid;
        }
        node->keys[i] = keys[pos_right - 1].first;
        node->data[i] = keys[pos_right - 1].second;
        node->children[i] = build_ideal_tree<K, V, MAX_KEYS>(keys, previous_pos + 1, pos_right - 1, pref_sum);
        previous_pos = pos_right - 1;
    }
    node->children[MAX_KEYS] = build_ideal_tree<K, V, MAX_KEYS>(keys, previous_pos + 1, r, pref_sum);

    return node;
}

namespace {
    template <typename K, typename V, int MAX_KEYS>
    void collect_keys(Node<K, V, MAX_KEYS>* node, std::vector<std::pair<K, ValueData<V>>>& keys) {
        if (node->children[0]) collect_keys(node->children[0], keys);
        for (int i = 0; i < node->size; i++) {
            if (node->data[i].accesses > 0 && (i == 0 || node->keys[i] != node->keys[i - 1]))
                keys.emplace_back(node->keys[i], node->data[i]);
            if (node->children[i + 1]) collect_keys(node->children[i + 1], keys);
        }
    }

    template <typename K, typename V, int MAX_KEYS>
    void delete_tree(Node<K, V, MAX_KEYS>* node) {
        for (int i = 0; i <= node->size; i++)
            if (node->children[i]) delete_tree(node->children[i]);
        delete node;
    }

    template <typename K, typename V, int MAX_KEYS>
    Node<K, V, MAX_KEYS>* rebuild_tree(Node<K, V, MAX_KEYS>* root) {
        std::vector<std::pair<K, ValueData<V>>> keys;
        collect_keys(root, keys);
        std::vector<int64_t> pref_sum(keys.size() + 1);
        pref_sum[0] = 0;
        for (int i = 0; i < keys.size(); i++)
            pref_sum[i + 1] = pref_sum[i] + keys[i].second.accesses;

        Node<K, V, MAX_KEYS>* result = build_ideal_tree<K, V, MAX_KEYS>(keys, 0, (int)keys.size(), pref_sum);

        delete_tree(root);

        return result;
    }
}


template <typename K, typename V, int MAX_KEYS>
class AdaptiveBTree {
    typedef Node<K, V, MAX_KEYS> AdaptiveNode;

 private:
    V noValue;
    AdaptiveNode* root;
 public:

    AdaptiveBTree(V noValue) : noValue(noValue), root(new AdaptiveNode(INITIAL_MAX_QUERIES)) {}

    V find(K const& key) {
        AdaptiveNode* node = root;
        AdaptiveNode** node_position = &root;
        AdaptiveNode* to_rebuild = nullptr;
        AdaptiveNode** to_rebuild_position;
        V result = noValue;
        while (node != nullptr) {
            if (node->new_query() && to_rebuild == nullptr) {
                to_rebuild = node;
                to_rebuild_position = node_position;
            }
            int pos = node->find_in_node(key);
            if (pos != node->size && node->keys[pos] == key) {
                if (node->data[pos].accesses < 0) break; // already removed
                node->data[pos].accesses++;
                result = node->data[pos].value;
                break;
            }
            node_position = &node->children[pos];
            node = node->children[pos];
        }
        if (to_rebuild != nullptr)
            *to_rebuild_position = rebuild_tree(to_rebuild);
        return result;
    }

    bool contains(K const& key) {
        return find(key) != noValue;
    }

    V insertIfAbsent(K const& key, V const& value) {
        AdaptiveNode* node = root;
        AdaptiveNode** node_position = &root;
        AdaptiveNode* to_rebuild = nullptr;
        AdaptiveNode** to_rebuild_position;
        V result = noValue;

        while (true) {
            if (node->new_query() && to_rebuild == nullptr) {
                to_rebuild = node;
                to_rebuild_position = node_position;
            }

            int pos = node->find_in_node(key);
            if (pos != node->size && node->keys[pos] == key) {
                if (node->data[pos].accesses < 0) {
                    node->data[pos].accesses = -node->data[pos].accesses;
                    node->data[pos].value = value;
                    break; // success
                }
                // already present
                node->data[pos].accesses++;
                result = node->data[pos].value;
                break;
            }
            if (!node->children[pos]) {
                if (node->size < MAX_KEYS) {
                    node->insert(key, value, pos);
                } else {
                    AdaptiveNode* new_child = new AdaptiveNode(INITIAL_MAX_QUERIES);
                    new_child->insert(key, value, 0);
                    node->children[pos] = new_child;
                }
                break; // success
            }

            node_position = &node->children[pos];
            node = node->children[pos];
        }

        if (to_rebuild != nullptr)
            *to_rebuild_position = rebuild_tree(to_rebuild);
        return result;
    }

    V erase(K const& key) {
        AdaptiveNode* node = root;
        AdaptiveNode** node_position = &root;
        AdaptiveNode* to_rebuild = nullptr;
        AdaptiveNode** to_rebuild_position;
        V result = noValue;

        while (node != nullptr) {
            if (node->new_query() && to_rebuild == nullptr) {
                to_rebuild = node;
                to_rebuild_position = node_position;
            }

            int pos = node->find_in_node(key);
            if (pos != node->size && node->keys[pos] == key) {
                if (node->data[pos].accesses < 0) {
                    break; // already removed
                }
                node->data[pos].accesses++;
                result = node->data[pos].value;
                node->remove(pos);
                break;
            }
            node_position = &node->children[pos];
            node = node->children[pos];
        }

        if (to_rebuild != nullptr)
            *to_rebuild_position = rebuild_tree(to_rebuild);
        return result;
    }

    void print_inner_structure() {
        print(root);
    }

    void print(AdaptiveNode* node) {
        if (!node) return;
        std::cout << node << " * " << node->size << std::endl;
        for (int i = 0; i < node->size; i++)
            std::cout << "(" << node->keys[i] << "," << node->data[i].value << "," << node->data[i].accesses << "); ";
        std::cout << endl;
        for (int i = 0; i <= node->size; i++)
            std::cout << node->children[i] << ", ";
        std::cout << endl;
        for (int i = 0; i <= node->size; i++)
            print(node->children[i]);
    }
};